let config = {
  room: {
      autoStart: true,
      roomName: `⚽ BORE | FUTSAL 3x3 ⚽`,
      geo: { "code": "BR", "lat": -29.74, "lon": -51.13 },
      noPlayer: true,
      maxPlayers: 16,
      public: true,
      roomScript: "/home/ubuntu/.haxroomie/room.js",
      token: 'thr1.AAAAAGYj3JXVvs4w4EoGvA.nGlGc5Gti9Q',
      hhm: "/home/ubuntu/.haxroomie/hhm-1.0.2.js"
  },
};
module.exports = config;